-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: pasarraya
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pr_t_admin`
--

DROP TABLE IF EXISTS `pr_t_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_admin` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varchar(100) NOT NULL,
  `s_username` varchar(40) NOT NULL,
  `s_password` char(60) NOT NULL,
  `s_email` varchar(100) DEFAULT NULL,
  `s_secret` varchar(40) DEFAULT NULL,
  `b_moderator` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_i_id`),
  UNIQUE KEY `s_username` (`s_username`),
  UNIQUE KEY `s_email` (`s_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_admin`
--

LOCK TABLES `pr_t_admin` WRITE;
/*!40000 ALTER TABLE `pr_t_admin` DISABLE KEYS */;
INSERT INTO `pr_t_admin` VALUES (1,'Administrator','admin','$2y$15$Mlugv1c.Zk/4IacOXrB0bObZB86dkHq/vNDNsGMIGxuefOan948qC','admin@pasarraya.com',NULL,0);
/*!40000 ALTER TABLE `pr_t_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_alerts`
--

DROP TABLE IF EXISTS `pr_t_alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_alerts` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_email` varchar(100) DEFAULT NULL,
  `fk_i_user_id` int(10) unsigned DEFAULT NULL,
  `s_search` longtext,
  `s_secret` varchar(40) DEFAULT NULL,
  `b_active` tinyint(1) NOT NULL DEFAULT '0',
  `e_type` enum('INSTANT','HOURLY','DAILY','WEEKLY','CUSTOM') NOT NULL,
  `dt_date` datetime DEFAULT NULL,
  `dt_unsub_date` datetime DEFAULT NULL,
  PRIMARY KEY (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_alerts`
--

LOCK TABLES `pr_t_alerts` WRITE;
/*!40000 ALTER TABLE `pr_t_alerts` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_alerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_alerts_sent`
--

DROP TABLE IF EXISTS `pr_t_alerts_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_alerts_sent` (
  `d_date` date NOT NULL,
  `i_num_alerts_sent` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`d_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_alerts_sent`
--

LOCK TABLES `pr_t_alerts_sent` WRITE;
/*!40000 ALTER TABLE `pr_t_alerts_sent` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_alerts_sent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_ban_rule`
--

DROP TABLE IF EXISTS `pr_t_ban_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_ban_rule` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varchar(250) NOT NULL DEFAULT '',
  `s_ip` varchar(50) NOT NULL DEFAULT '',
  `s_email` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_ban_rule`
--

LOCK TABLES `pr_t_ban_rule` WRITE;
/*!40000 ALTER TABLE `pr_t_ban_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_ban_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_category`
--

DROP TABLE IF EXISTS `pr_t_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_category` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_i_parent_id` int(10) unsigned DEFAULT NULL,
  `i_expiration_days` int(3) unsigned NOT NULL DEFAULT '0',
  `i_position` int(2) unsigned NOT NULL DEFAULT '0',
  `b_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `b_price_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `s_icon` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`pk_i_id`),
  KEY `fk_i_parent_id` (`fk_i_parent_id`),
  KEY `i_position` (`i_position`),
  CONSTRAINT `pr_t_category_ibfk_1` FOREIGN KEY (`fk_i_parent_id`) REFERENCES `pr_t_category` (`pk_i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_category`
--

LOCK TABLES `pr_t_category` WRITE;
/*!40000 ALTER TABLE `pr_t_category` DISABLE KEYS */;
INSERT INTO `pr_t_category` VALUES (1,NULL,0,1,1,1,NULL),(10,1,0,2,1,1,NULL),(96,NULL,0,0,1,1,NULL),(97,96,0,2,1,1,NULL),(98,97,0,0,1,1,NULL),(99,96,0,1,1,1,NULL),(100,99,0,0,1,1,NULL),(103,96,0,0,1,1,NULL),(104,103,0,0,1,1,NULL);
/*!40000 ALTER TABLE `pr_t_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_category_description`
--

DROP TABLE IF EXISTS `pr_t_category_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_category_description` (
  `fk_i_category_id` int(10) unsigned NOT NULL,
  `fk_c_locale_code` char(5) NOT NULL,
  `s_name` varchar(100) DEFAULT NULL,
  `s_description` text,
  `s_slug` varchar(100) NOT NULL,
  PRIMARY KEY (`fk_i_category_id`,`fk_c_locale_code`),
  KEY `idx_s_slug` (`s_slug`),
  KEY `fk_c_locale_code` (`fk_c_locale_code`),
  CONSTRAINT `pr_t_category_description_ibfk_1` FOREIGN KEY (`fk_i_category_id`) REFERENCES `pr_t_category` (`pk_i_id`),
  CONSTRAINT `pr_t_category_description_ibfk_2` FOREIGN KEY (`fk_c_locale_code`) REFERENCES `pr_t_locale` (`pk_c_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_category_description`
--

LOCK TABLES `pr_t_category_description` WRITE;
/*!40000 ALTER TABLE `pr_t_category_description` DISABLE KEYS */;
INSERT INTO `pr_t_category_description` VALUES (96,'id_ID','Bahan Baku','','bahan-baku'),(97,'en_US','Makanan','','makanan'),(97,'id_ID','Sayuran','','sayuran'),(98,'id_ID','Tomat','','tomat'),(99,'id_ID','Buah Buahan','','buah-buahan'),(100,'id_ID','Apel','','apel'),(103,'en_US','Daging','','daging'),(103,'id_ID','Daging','','daging'),(104,'id_ID','Sapi','','sapi');
/*!40000 ALTER TABLE `pr_t_category_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_category_stats`
--

DROP TABLE IF EXISTS `pr_t_category_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_category_stats` (
  `fk_i_category_id` int(10) unsigned NOT NULL,
  `i_num_items` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fk_i_category_id`),
  CONSTRAINT `pr_t_category_stats_ibfk_1` FOREIGN KEY (`fk_i_category_id`) REFERENCES `pr_t_category` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_category_stats`
--

LOCK TABLES `pr_t_category_stats` WRITE;
/*!40000 ALTER TABLE `pr_t_category_stats` DISABLE KEYS */;
INSERT INTO `pr_t_category_stats` VALUES (10,0),(96,0),(97,0),(98,0),(99,0),(100,0),(103,0),(104,0);
/*!40000 ALTER TABLE `pr_t_category_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_city`
--

DROP TABLE IF EXISTS `pr_t_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_city` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_i_region_id` int(10) unsigned NOT NULL,
  `s_name` varchar(60) NOT NULL,
  `s_slug` varchar(60) NOT NULL DEFAULT '',
  `fk_c_country_code` char(2) DEFAULT NULL,
  `b_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pk_i_id`),
  KEY `fk_i_region_id` (`fk_i_region_id`),
  KEY `idx_s_name` (`s_name`),
  KEY `idx_s_slug` (`s_slug`),
  KEY `fk_c_country_code` (`fk_c_country_code`),
  CONSTRAINT `pr_t_city_ibfk_1` FOREIGN KEY (`fk_i_region_id`) REFERENCES `pr_t_region` (`pk_i_id`),
  CONSTRAINT `pr_t_city_ibfk_2` FOREIGN KEY (`fk_c_country_code`) REFERENCES `pr_t_country` (`pk_c_code`)
) ENGINE=InnoDB AUTO_INCREMENT=274631 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_city`
--

LOCK TABLES `pr_t_city` WRITE;
/*!40000 ALTER TABLE `pr_t_city` DISABLE KEYS */;
INSERT INTO `pr_t_city` VALUES (274165,781431,'Aceh Besar','aceh-besar','id',1),(274166,781431,'Aceh Jaya','aceh-jaya','id',1),(274167,781431,'Aceh Singkil','aceh-singkil','id',1),(274168,781431,'Aceh Tamiang','aceh-tamiang','id',1),(274169,781431,'Bener Meriah','bener-meriah','id',1),(274170,781431,'Bireuën','bireuen','id',1),(274171,781431,'Central Aceh','central-aceh','id',1),(274172,781431,'East Aceh','east-aceh','id',1),(274173,781431,'Gayo Lues','gayo-lues','id',1),(274174,781431,'Nagan Raya','nagan-raya','id',1),(274175,781431,'North Aceh','north-aceh','id',1),(274176,781431,'Pidie','pidie','id',1),(274177,781431,'Pidie Jaya','pidie-jaya','id',1),(274178,781431,'Simeulue','simeulue','id',1),(274179,781431,'South Aceh','south-aceh','id',1),(274180,781431,'Southeast Aceh','southeast-aceh','id',1),(274181,781431,'Southwest Aceh','southwest-aceh','id',1),(274182,781431,'West Aceh','west-aceh','id',1),(274183,781431,'Banda Aceh','banda-aceh','id',1),(274184,781431,'Langsa','langsa','id',1),(274185,781431,'Lhokseumawe','lhokseumawe','id',1),(274186,781431,'Sabang','sabang','id',1),(274187,781431,'Subulussalam','subulussalam','id',1),(274188,781432,'Badung','badung','id',1),(274189,781432,'Bangli','bangli','id',1),(274190,781432,'Buleleng','buleleng','id',1),(274191,781432,'Gianyar','gianyar','id',1),(274192,781432,'Jembrana','jembrana','id',1),(274193,781432,'Karangasem','karangasem','id',1),(274194,781432,'Klungkung','klungkung','id',1),(274195,781432,'Tabanan','tabanan','id',1),(274196,781432,'Denpasar','denpasar','id',1),(274197,781433,'Bangka','bangka','id',1),(274198,781433,'Belitung','belitung','id',1),(274199,781433,'Central Bangka','central-bangka','id',1),(274200,781433,'East Belitung','east-belitung','id',1),(274201,781433,'South Bangka','south-bangka','id',1),(274202,781433,'West Bangka','west-bangka','id',1),(274203,781433,'Pangkal Pinang','pangkal-pinang','id',1),(274204,781434,'Lebak','lebak','id',1),(274205,781434,'Pandeglang','pandeglang','id',1),(274206,781434,'Serang','serang','id',1),(274207,781434,'Cilegon','cilegon','id',1),(274208,781434,'South Tangerang','south-tangerang','id',1),(274209,781434,'Tangerang','tangerang','id',1),(274210,781435,'Central Bengkulu','central-bengkulu','id',1),(274211,781435,'Kaur','kaur','id',1),(274212,781435,'Kepahiang','kepahiang','id',1),(274213,781435,'Lebong','lebong','id',1),(274214,781435,'Muko-Muko','muko-muko','id',1),(274215,781435,'North Bengkulu','north-bengkulu','id',1),(274216,781435,'Rejang Lebong','rejang-lebong','id',1),(274217,781435,'Seluma','seluma','id',1),(274218,781435,'South Bengkulu','south-bengkulu','id',1),(274219,781435,'Bengkulu','bengkulu','id',1),(274220,781436,'Banjarnegara','banjarnegara','id',1),(274221,781436,'Banyumas','banyumas','id',1),(274222,781436,'Batang','batang','id',1),(274223,781436,'Blora Regency','blora-regency','id',1),(274224,781436,'Boyolali','boyolali','id',1),(274225,781436,'Brebes','brebes','id',1),(274226,781436,'Cilacap','cilacap','id',1),(274227,781436,'Demak','demak','id',1),(274228,781436,'Grobogan','grobogan','id',1),(274229,781436,'Jepara','jepara','id',1),(274230,781436,'Karanganyar','karanganyar','id',1),(274231,781436,'Kebumen','kebumen','id',1),(274232,781436,'Kendal','kendal','id',1),(274233,781436,'Klaten','klaten','id',1),(274234,781436,'Magelang','magelang','id',1),(274235,781436,'Pati','pati','id',1),(274236,781436,'Pekalongan','pekalongan','id',1),(274237,781436,'Pemalang','pemalang','id',1),(274238,781436,'Purbalingga','purbalingga','id',1),(274239,781436,'Purworejo','purworejo','id',1),(274240,781436,'Rembang','rembang','id',1),(274241,781436,'Semarang','semarang','id',1),(274242,781436,'Sragen','sragen','id',1),(274243,781436,'Sukoharjo','sukoharjo','id',1),(274244,781436,'Tegal','tegal','id',1),(274245,781436,'Temanggung','temanggung','id',1),(274246,781436,'Wonogiri','wonogiri','id',1),(274247,781436,'Wonosobo','wonosobo','id',1),(274248,781436,'Surakarta','surakarta','id',1),(274249,781436,'Salatiga','salatiga','id',1),(274250,781436,'Kudus','kudus','id',1),(274251,781437,'East Barito','east-barito','id',1),(274252,781437,'East Kotawaringin','east-kotawaringin','id',1),(274253,781437,'Gunung Mas','gunung-mas','id',1),(274254,781437,'Kapuas','kapuas','id',1),(274255,781437,'Katingan','katingan','id',1),(274256,781437,'Lamandau','lamandau','id',1),(274257,781437,'Murung Raya','murung-raya','id',1),(274258,781437,'North Barito','north-barito','id',1),(274259,781437,'Pulang Pisang','pulang-pisang','id',1),(274260,781437,'Sukamara','sukamara','id',1),(274261,781437,'Seruyan','seruyan','id',1),(274262,781437,'South Barito','south-barito','id',1),(274263,781437,'West Kotawaringin','west-kotawaringin','id',1),(274264,781437,'Palangkaraya','palangkaraya','id',1),(274265,781438,'Banggai','banggai','id',1),(274266,781438,'Banggai Islands','banggai-islands','id',1),(274267,781438,'Buol','buol','id',1),(274268,781438,'Donggala','donggala','id',1),(274269,781438,'Morowali','morowali','id',1),(274270,781438,'Parigi Moutong','parigi-moutong','id',1),(274271,781438,'Poso','poso','id',1),(274272,781438,'Sigi','sigi','id',1),(274273,781438,'Tojo Una-Una','tojo-una-una','id',1),(274274,781438,'Toli-Toli','toli-toli','id',1),(274275,781438,'Palu','palu','id',1),(274276,781439,'Bangkalan Regency','bangkalan-regency','id',1),(274277,781439,'Banyuwangi','banyuwangi','id',1),(274278,781439,'Blitar','blitar','id',1),(274279,781439,'Bojonegoro','bojonegoro','id',1),(274280,781439,'Bondowoso','bondowoso','id',1),(274281,781439,'Gresik','gresik','id',1),(274282,781439,'Jember','jember','id',1),(274283,781439,'Jombang','jombang','id',1),(274284,781439,'Kediri','kediri','id',1),(274285,781439,'Lamongan','lamongan','id',1),(274286,781439,'Lumajang','lumajang','id',1),(274287,781439,'Madiun','madiun','id',1),(274288,781439,'Magetan','magetan','id',1),(274289,781439,'Malang','malang','id',1),(274290,781439,'Mojokerto','mojokerto','id',1),(274291,781439,'Ngawi Regency','ngawi-regency','id',1),(274292,781439,'Pacitan','pacitan','id',1),(274293,781439,'Pasuruan','pasuruan','id',1),(274294,781439,'Ponorogo','ponorogo','id',1),(274295,781439,'Probolinggo','probolinggo','id',1),(274296,781439,'Sampang','sampang','id',1),(274297,781439,'Sidoarjo','sidoarjo','id',1),(274298,781439,'Situbondo','situbondo','id',1),(274299,781439,'Sumenep','sumenep','id',1),(274300,781439,'Trenggalek','trenggalek','id',1),(274301,781439,'Tuban','tuban','id',1),(274302,781439,'Tulungagung','tulungagung','id',1),(274303,781439,'Batu','batu','id',1),(274304,781439,'Surabaya','surabaya','id',1),(274305,781439,'Nganjuk','nganjuk','id',1),(274306,781439,'Pamekasan','pamekasan','id',1),(274307,781440,'Berau','berau','id',1),(274308,781440,'Bulungan','bulungan','id',1),(274309,781440,'East Kutai','east-kutai','id',1),(274310,781440,'Kutai Kartanegara','kutai-kartanegara','id',1),(274311,781440,'Malinau','malinau','id',1),(274312,781440,'North Penajam Paser','north-penajam-paser','id',1),(274313,781440,'Nunukan','nunukan','id',1),(274314,781440,'Paser','paser','id',1),(274315,781440,'Tana Tidung','tana-tidung','id',1),(274316,781440,'West Kutai','west-kutai','id',1),(274317,781440,'Tarakan','tarakan','id',1),(274318,781440,'Bontang','bontang','id',1),(274319,781440,'Samarinda','samarinda','id',1),(274320,781440,'Balikpapan','balikpapan','id',1),(274321,781441,'Alor','alor','id',1),(274322,781441,'Belu','belu','id',1),(274323,781441,'Central Sumba','central-sumba','id',1),(274324,781441,'East Flores','east-flores','id',1),(274325,781441,'East Manggarai','east-manggarai','id',1),(274326,781441,'East Sumba','east-sumba','id',1),(274327,781441,'Ende','ende','id',1),(274328,781441,'Kupang','kupang','id',1),(274329,781441,'Lembata','lembata','id',1),(274330,781441,'Manggarai','manggarai','id',1),(274331,781441,'Nagekeo','nagekeo','id',1),(274332,781441,'Ngada','ngada','id',1),(274333,781441,'North Central Timor','north-central-timor','id',1),(274334,781441,'Rote Ndao','rote-ndao','id',1),(274335,781441,'Sabu Raijua','sabu-raijua','id',1),(274336,781441,'Sikka','sikka','id',1),(274337,781441,'South Central Timor','south-central-timor','id',1),(274338,781441,'Southwest Sumba','southwest-sumba','id',1),(274339,781441,'West Manggarai','west-manggarai','id',1),(274340,781441,'West Sumba','west-sumba','id',1),(274341,781442,'Boalemo','boalemo','id',1),(274342,781442,'Bone Bolango','bone-bolango','id',1),(274343,781442,'Gorontalo','gorontalo','id',1),(274344,781442,'North Gorontalo','north-gorontalo','id',1),(274345,781442,'Pahuwato','pahuwato','id',1),(274346,781443,'Seribu Islands','seribu-islands','id',1),(274347,781443,'Jakarta','jakarta','id',1),(274348,781444,'Batang Hari','batang-hari','id',1),(274349,781444,'Bungo','bungo','id',1),(274350,781444,'Kerinci','kerinci','id',1),(274351,781444,'Merangin','merangin','id',1),(274352,781444,'Muaro Jambi','muaro-jambi','id',1),(274353,781444,'Sarolangun','sarolangun','id',1),(274354,781444,'East Tanjung Jabung','east-tanjung-jabung','id',1),(274355,781444,'West Tanjung Jabung','west-tanjung-jabung','id',1),(274356,781444,'Tebo','tebo','id',1),(274357,781444,'Jambi','jambi','id',1),(274358,781444,'Sungai Penuh','sungai-penuh','id',1),(274359,781445,'Central Lampung','central-lampung','id',1),(274360,781445,'East Lampung','east-lampung','id',1),(274361,781445,'Mesuji','mesuji','id',1),(274362,781445,'North Lampung','north-lampung','id',1),(274363,781445,'Pesawaran','pesawaran','id',1),(274364,781445,'Pringsewu','pringsewu','id',1),(274365,781445,'South Lampung','south-lampung','id',1),(274366,781445,'Tanggamus','tanggamus','id',1),(274367,781445,'Tulang Bawang','tulang-bawang','id',1),(274368,781445,'Way Kanan','way-kanan','id',1),(274369,781445,'West Lampung','west-lampung','id',1),(274370,781445,'West Tulang Bawang','west-tulang-bawang','id',1),(274371,781445,'Metro','metro','id',1),(274372,781445,'Bandar Lampung','bandar-lampung','id',1),(274373,781446,'Aru Islands','aru-islands','id',1),(274374,781446,'Buru','buru','id',1),(274375,781446,'Central Maluku','central-maluku','id',1),(274376,781446,'Eastern Seram','eastern-seram','id',1),(274377,781446,'South Buru','south-buru','id',1),(274378,781446,'Southeast Maluku','southeast-maluku','id',1),(274379,781446,'Southwest Maluku','southwest-maluku','id',1),(274380,781446,'Western Seram','western-seram','id',1),(274381,781446,'Western Southeast Maluku','western-southeast-maluku','id',1),(274382,781446,'Ambon','ambon','id',1),(274383,781446,'Tual','tual','id',1),(274384,781447,'Central Halmahera','central-halmahera','id',1),(274385,781447,'East Halmahera','east-halmahera','id',1),(274386,781447,'Morotai Islands','morotai-islands','id',1),(274387,781447,'North Halmahera','north-halmahera','id',1),(274388,781447,'South Halmahera','south-halmahera','id',1),(274389,781447,'Sula Islands','sula-islands','id',1),(274390,781447,'West Halmahera','west-halmahera','id',1),(274391,781447,'Ternate','ternate','id',1),(274392,781447,'Tidore Islands','tidore-islands','id',1),(274393,781448,'Bolaang Mongondow Regency','bolaang-mongondow-regency','id',1),(274394,781448,'East Bolaang Mongondow Regency','east-bolaang-mongondow-regency','id',1),(274395,781448,'Minahasa Regency','minahasa-regency','id',1),(274396,781448,'North Bolaang Mongondow Regency','north-bolaang-mongondow-regency','id',1),(274397,781448,'North Minahasa Regency','north-minahasa-regency','id',1),(274398,781448,'Sangihe Islands Regency','sangihe-islands-regency','id',1),(274399,781448,'Sitaro Islands Regency','sitaro-islands-regency','id',1),(274400,781448,'South Bolaang Mongondow Regency','south-bolaang-mongondow-regency','id',1),(274401,781448,'South Minahasa Regency','south-minahasa-regency','id',1),(274402,781448,'Southeast Minahasa Regency','southeast-minahasa-regency','id',1),(274403,781448,'Talaud Islands Regency','talaud-islands-regency','id',1),(274404,781448,'Bitung','bitung','id',1),(274405,781448,'Kotamobagu','kotamobagu','id',1),(274406,781448,'Tomohon','tomohon','id',1),(274407,781448,'Manado','manado','id',1),(274408,781449,'Asahan','asahan','id',1),(274409,781449,'Batubara','batubara','id',1),(274410,781449,'Central Tapanuli','central-tapanuli','id',1),(274411,781449,'Dairi','dairi','id',1),(274412,781449,'Humbang Hasundutan','humbang-hasundutan','id',1),(274413,781449,'Karo','karo','id',1),(274414,781449,'Labuhan Batu','labuhan-batu','id',1),(274415,781449,'Langkat','langkat','id',1),(274416,781449,'Mandailing Natal','mandailing-natal','id',1),(274417,781449,'Nias','nias','id',1),(274418,781449,'North Labuhan Batu','north-labuhan-batu','id',1),(274419,781449,'North Nias','north-nias','id',1),(274420,781449,'North Padang Lawas','north-padang-lawas','id',1),(274421,781449,'North Tapanuli','north-tapanuli','id',1),(274422,781449,'Padang Lawas','padang-lawas','id',1),(274423,781449,'Pakpak Bharat','pakpak-bharat','id',1),(274424,781449,'Samosir','samosir','id',1),(274425,781449,'Serdang Bedagai','serdang-bedagai','id',1),(274426,781449,'Simalungun','simalungun','id',1),(274427,781449,'South Labuhan Batu','south-labuhan-batu','id',1),(274428,781449,'South Nias','south-nias','id',1),(274429,781449,'South Tapanuli','south-tapanuli','id',1),(274430,781449,'Toba Samosir','toba-samosir','id',1),(274431,781449,'West Nias','west-nias','id',1),(274432,781449,'Binjai','binjai','id',1),(274433,781449,'Gunungsitoli','gunungsitoli','id',1),(274434,781449,'Medan','medan','id',1),(274435,781449,'Padang Sidempuan','padang-sidempuan','id',1),(274436,781449,'Sibolga','sibolga','id',1),(274437,781449,'Tanjung Balai','tanjung-balai','id',1),(274438,781449,'Tebing Tinggi','tebing-tinggi','id',1),(274439,781449,'Pematangsiantar','pematangsiantar','id',1),(274440,781449,'Deli Serdang','deli-serdang','id',1),(274441,781450,'Asmat','asmat','id',1),(274442,781450,'Biak Numfor','biak-numfor','id',1),(274443,781450,'Boven Digoel','boven-digoel','id',1),(274444,781450,'Central Mamberamo','central-mamberamo','id',1),(274445,781450,'Deiyai','deiyai','id',1),(274446,781450,'Dogiyai','dogiyai','id',1),(274447,781450,'Intan Jaya','intan-jaya','id',1),(274448,781450,'Jayawijaya','jayawijaya','id',1),(274449,781450,'Keerom','keerom','id',1),(274450,781450,'Lanny Jaya','lanny-jaya','id',1),(274451,781450,'Mamberamo Jaya','mamberamo-jaya','id',1),(274452,781450,'Mappi','mappi','id',1),(274453,781450,'Merauke','merauke','id',1),(274454,781450,'Mimika','mimika','id',1),(274455,781450,'Nabire','nabire','id',1),(274456,781450,'Nduga','nduga','id',1),(274457,781450,'Paniai','paniai','id',1),(274458,781450,'Bintang Mountains','bintang-mountains','id',1),(274459,781450,'Puncak','puncak','id',1),(274460,781450,'Puncak Jaya','puncak-jaya','id',1),(274461,781450,'Sarmi','sarmi','id',1),(274462,781450,'Supiori','supiori','id',1),(274463,781450,'Tolikara','tolikara','id',1),(274464,781450,'Waropen','waropen','id',1),(274465,781450,'Yahukimo','yahukimo','id',1),(274466,781450,'Yalimo','yalimo','id',1),(274467,781450,'Yapen Islands','yapen-islands','id',1),(274468,781450,'Jayapura','jayapura','id',1),(274469,781451,'Bengkalis Regency','bengkalis-regency','id',1),(274470,781451,'Indragiri Hilir','indragiri-hilir','id',1),(274471,781451,'Indragiri Hulu Regency','indragiri-hulu-regency','id',1),(274472,781451,'Kampar Regency','kampar-regency','id',1),(274473,781451,'Kuantan Singingi','kuantan-singingi','id',1),(274474,781451,'Meranti Islands','meranti-islands','id',1),(274475,781451,'Pelalawan','pelalawan','id',1),(274476,781451,'Rokan Hulu','rokan-hulu','id',1),(274477,781451,'Rokan Hilir','rokan-hilir','id',1),(274478,781451,'Siak Regency','siak-regency','id',1),(274479,781451,'Dumai','dumai','id',1),(274480,781451,'Pekanbaru','pekanbaru','id',1),(274481,781452,'Anambas Islands','anambas-islands','id',1),(274482,781452,'Bintan','bintan','id',1),(274483,781452,'Karimun','karimun','id',1),(274484,781452,'Lingga','lingga','id',1),(274485,781452,'Natuna','natuna','id',1),(274486,781452,'Batam','batam','id',1),(274487,781452,'Tanjung Pinang','tanjung-pinang','id',1),(274488,781453,'Bombana','bombana','id',1),(274489,781453,'Buton','buton','id',1),(274490,781453,'Kolaka','kolaka','id',1),(274491,781453,'Konawe','konawe','id',1),(274492,781453,'Muna','muna','id',1),(274493,781453,'North Buton','north-buton','id',1),(274494,781453,'North Kolaka','north-kolaka','id',1),(274495,781453,'North Konawe','north-konawe','id',1),(274496,781453,'South Konawe','south-konawe','id',1),(274497,781453,'Wakatobi','wakatobi','id',1),(274498,781453,'Bau-Bau','bau-bau','id',1),(274499,781453,'Kendari','kendari','id',1),(274500,781454,'Balangan','balangan','id',1),(274501,781454,'Banjar','banjar','id',1),(274502,781454,'Barito Kuala','barito-kuala','id',1),(274503,781454,'Central Hulu Sungai','central-hulu-sungai','id',1),(274504,781454,'Kotabaru','kotabaru','id',1),(274505,781454,'North Hulu Sungai','north-hulu-sungai','id',1),(274506,781454,'South Hulu Sungai','south-hulu-sungai','id',1),(274507,781454,'Tabalong','tabalong','id',1),(274508,781454,'Tanah Laut','tanah-laut','id',1),(274509,781454,'Tanah Bumbu','tanah-bumbu','id',1),(274510,781454,'Tapin','tapin','id',1),(274511,781454,'Banjarbaru','banjarbaru','id',1),(274512,781454,'Banjarmasin','banjarmasin','id',1),(274513,781455,'Bantaeng','bantaeng','id',1),(274514,781455,'Barru','barru','id',1),(274515,781455,'Bone','bone','id',1),(274516,781455,'Bulukumba','bulukumba','id',1),(274517,781455,'East Luwu','east-luwu','id',1),(274518,781455,'Enrekang','enrekang','id',1),(274519,781455,'Gowa','gowa','id',1),(274520,781455,'Jeneponto','jeneponto','id',1),(274521,781455,'Luwu','luwu','id',1),(274522,781455,'North Luwu','north-luwu','id',1),(274523,781455,'Maros','maros','id',1),(274524,781455,'Pangkajene Islands','pangkajene-islands','id',1),(274525,781455,'Pinrang','pinrang','id',1),(274526,781455,'Selayar Islands','selayar-islands','id',1),(274527,781455,'Sidenreng Rappang','sidenreng-rappang','id',1),(274528,781455,'Soppeng','soppeng','id',1),(274529,781455,'Takalar','takalar','id',1),(274530,781455,'Tana Toraja','tana-toraja','id',1),(274531,781455,'Wajo','wajo','id',1),(274532,781455,'Palopo','palopo','id',1),(274533,781455,'Pare-Pare','pare-pare','id',1),(274534,781455,'Sinjai','sinjai','id',1),(274535,781455,'Makassar','makassar','id',1),(274536,781456,'Banyuasin','banyuasin','id',1),(274537,781456,'East Ogan Komering Ulu','east-ogan-komering-ulu','id',1),(274538,781456,'Empat Lawang','empat-lawang','id',1),(274539,781456,'Muarama Enim','muarama-enim','id',1),(274540,781456,'Musi Banyuasin','musi-banyuasin','id',1),(274541,781456,'Musi Rawas','musi-rawas','id',1),(274542,781456,'Ogan Ilir','ogan-ilir','id',1),(274543,781456,'Ogan Komering Ilir','ogan-komering-ilir','id',1),(274544,781456,'Ogan Komering Ulu','ogan-komering-ulu','id',1),(274545,781456,'South Ogan Komering Ulu','south-ogan-komering-ulu','id',1),(274546,781456,'Pagar Alam','pagar-alam','id',1),(274547,781456,'Palembang','palembang','id',1),(274548,781456,'Lahat','lahat','id',1),(274549,781456,'Prabumulih','prabumulih','id',1),(274550,781456,'Lubuklinggau','lubuklinggau','id',1),(274551,781457,'Bandung','bandung','id',1),(274552,781457,'Bekasi','bekasi','id',1),(274553,781457,'Bogor','bogor','id',1),(274554,781457,'Ciamis','ciamis','id',1),(274555,781457,'Cianjur','cianjur','id',1),(274556,781457,'Cirebon','cirebon','id',1),(274557,781457,'Garut','garut','id',1),(274558,781457,'Karawang','karawang','id',1),(274559,781457,'Kuningan','kuningan','id',1),(274560,781457,'Subang','subang','id',1),(274561,781457,'Sukabumi','sukabumi','id',1),(274562,781457,'Sumedang','sumedang','id',1),(274563,781457,'West Bandung','west-bandung','id',1),(274564,781457,'Banjar','banjar','id',1),(274565,781457,'Cimahi','cimahi','id',1),(274566,781457,'Depok','depok','id',1),(274567,781457,'Majalengka','majalengka','id',1),(274568,781457,'Indramayu','indramayu','id',1),(274569,781457,'Purwakarta','purwakarta','id',1),(274570,781457,'Tasikmalaya','tasikmalaya','id',1),(274571,781458,'Bengkayang Regency','bengkayang-regency','id',1),(274572,781458,'Kapuas Hulu Regency','kapuas-hulu-regency','id',1),(274573,781458,'North Kayong Regency','north-kayong-regency','id',1),(274574,781458,'Ketapang Regency','ketapang-regency','id',1),(274575,781458,'Kubu Raya Regency','kubu-raya-regency','id',1),(274576,781458,'Landak Regency','landak-regency','id',1),(274577,781458,'Melawi Regency','melawi-regency','id',1),(274578,781458,'Sambas Regency','sambas-regency','id',1),(274579,781458,'Sanggau Regency','sanggau-regency','id',1),(274580,781458,'Sekadau Regency','sekadau-regency','id',1),(274581,781458,'Sintang Regency','sintang-regency','id',1),(274582,781458,'Singkawang','singkawang','id',1),(274583,781458,'Pontianak','pontianak','id',1),(274584,781459,'Bima','bima','id',1),(274585,781459,'Central Lombok','central-lombok','id',1),(274586,781459,'Dompu','dompu','id',1),(274587,781459,'East Lombok','east-lombok','id',1),(274588,781459,'North Lombok','north-lombok','id',1),(274589,781459,'Sumbawa','sumbawa','id',1),(274590,781459,'West Lombok','west-lombok','id',1),(274591,781459,'West Sumbawa','west-sumbawa','id',1),(274592,781459,'Mataram','mataram','id',1),(274593,781460,'Fak-Fak','fak-fak','id',1),(274594,781460,'Kaimana','kaimana','id',1),(274595,781460,'Maybrat','maybrat','id',1),(274596,781460,'Raja Ampat','raja-ampat','id',1),(274597,781460,'Sorong','sorong','id',1),(274598,781460,'South Sorong','south-sorong','id',1),(274599,781460,'Tambrauw','tambrauw','id',1),(274600,781460,'Teluk Bintuni','teluk-bintuni','id',1),(274601,781460,'Teluk Wondama','teluk-wondama','id',1),(274602,781460,'Manokwari','manokwari','id',1),(274603,781461,'Majene','majene','id',1),(274604,781461,'Mamasa','mamasa','id',1),(274605,781461,'Mamuju','mamuju','id',1),(274606,781461,'North Mamuju','north-mamuju','id',1),(274607,781461,'Polewali Mandar','polewali-mandar','id',1),(274608,781462,'Agam','agam','id',1),(274609,781462,'Dharmasraya','dharmasraya','id',1),(274610,781462,'Lima Puluh Kota','lima-puluh-kota','id',1),(274611,781462,'Mentawai Islands','mentawai-islands','id',1),(274612,781462,'Padang Pariaman','padang-pariaman','id',1),(274613,781462,'Pasaman','pasaman','id',1),(274614,781462,'Sawahlunto Sijunjung','sawahlunto-sijunjung','id',1),(274615,781462,'Solok','solok','id',1),(274616,781462,'South Pesisir','south-pesisir','id',1),(274617,781462,'South Solok','south-solok','id',1),(274618,781462,'Tanah Datar','tanah-datar','id',1),(274619,781462,'West Pasaman','west-pasaman','id',1),(274620,781462,'Bukittinggi','bukittinggi','id',1),(274621,781462,'Padang','padang','id',1),(274622,781462,'Padang Panjang','padang-panjang','id',1),(274623,781462,'Pariaman','pariaman','id',1),(274624,781462,'Payakumbuh','payakumbuh','id',1),(274625,781462,'Sawahlunto','sawahlunto','id',1),(274626,781463,'Bantul','bantul','id',1),(274627,781463,'Gunung Kidul','gunung-kidul','id',1),(274628,781463,'Kulon Progo','kulon-progo','id',1),(274629,781463,'Sleman','sleman','id',1),(274630,781463,'Yogyakarta','yogyakarta','id',1);
/*!40000 ALTER TABLE `pr_t_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_city_area`
--

DROP TABLE IF EXISTS `pr_t_city_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_city_area` (
  `pk_i_id` int(10) unsigned NOT NULL,
  `fk_i_city_id` int(10) unsigned NOT NULL,
  `s_name` varchar(255) NOT NULL,
  PRIMARY KEY (`pk_i_id`),
  KEY `fk_i_city_id` (`fk_i_city_id`),
  KEY `idx_s_name` (`s_name`),
  CONSTRAINT `pr_t_city_area_ibfk_1` FOREIGN KEY (`fk_i_city_id`) REFERENCES `pr_t_city` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_city_area`
--

LOCK TABLES `pr_t_city_area` WRITE;
/*!40000 ALTER TABLE `pr_t_city_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_city_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_city_stats`
--

DROP TABLE IF EXISTS `pr_t_city_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_city_stats` (
  `fk_i_city_id` int(10) unsigned NOT NULL,
  `i_num_items` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fk_i_city_id`),
  KEY `idx_num_items` (`i_num_items`),
  CONSTRAINT `pr_t_city_stats_ibfk_1` FOREIGN KEY (`fk_i_city_id`) REFERENCES `pr_t_city` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_city_stats`
--

LOCK TABLES `pr_t_city_stats` WRITE;
/*!40000 ALTER TABLE `pr_t_city_stats` DISABLE KEYS */;
INSERT INTO `pr_t_city_stats` VALUES (274165,0),(274166,0),(274167,0),(274168,0),(274169,0),(274170,0),(274171,0),(274172,0),(274173,0),(274174,0),(274175,0),(274176,0),(274177,0),(274178,0),(274179,0),(274180,0),(274181,0),(274182,0),(274183,0),(274184,0),(274185,0),(274186,0),(274187,0),(274188,0),(274189,0),(274190,0),(274191,0),(274192,0),(274193,0),(274194,0),(274195,0),(274196,0),(274197,0),(274198,0),(274199,0),(274200,0),(274201,0),(274202,0),(274203,0),(274204,0),(274205,0),(274206,0),(274207,0),(274208,0),(274209,0),(274210,0),(274211,0),(274212,0),(274213,0),(274214,0),(274215,0),(274216,0),(274217,0),(274218,0),(274219,0),(274220,0),(274221,0),(274222,0),(274223,0),(274224,0),(274225,0),(274226,0),(274227,0),(274228,0),(274229,0),(274230,0),(274231,0),(274232,0),(274233,0),(274234,0),(274235,0),(274236,0),(274237,0),(274238,0),(274239,0),(274240,0),(274241,0),(274242,0),(274243,0),(274244,0),(274245,0),(274246,0),(274247,0),(274248,0),(274249,0),(274250,0),(274251,0),(274252,0),(274253,0),(274254,0),(274255,0),(274256,0),(274257,0),(274258,0),(274259,0),(274260,0),(274261,0),(274262,0),(274263,0),(274264,0),(274265,0),(274266,0),(274267,0),(274268,0),(274269,0),(274270,0),(274271,0),(274272,0),(274273,0),(274274,0),(274275,0),(274276,0),(274277,0),(274278,0),(274279,0),(274280,0),(274281,0),(274282,0),(274283,0),(274284,0),(274285,0),(274286,0),(274287,0),(274288,0),(274289,0),(274290,0),(274291,0),(274292,0),(274293,0),(274294,0),(274295,0),(274296,0),(274297,0),(274298,0),(274299,0),(274300,0),(274301,0),(274302,0),(274303,0),(274304,0),(274305,0),(274306,0),(274307,0),(274308,0),(274309,0),(274310,0),(274311,0),(274312,0),(274313,0),(274314,0),(274315,0),(274316,0),(274317,0),(274318,0),(274319,0),(274320,0),(274321,0),(274322,0),(274323,0),(274324,0),(274325,0),(274326,0),(274327,0),(274328,0),(274329,0),(274330,0),(274331,0),(274332,0),(274333,0),(274334,0),(274335,0),(274336,0),(274337,0),(274338,0),(274339,0),(274340,0),(274341,0),(274342,0),(274343,0),(274344,0),(274345,0),(274346,0),(274347,0),(274348,0),(274349,0),(274350,0),(274351,0),(274352,0),(274353,0),(274354,0),(274355,0),(274356,0),(274357,0),(274358,0),(274359,0),(274360,0),(274361,0),(274362,0),(274363,0),(274364,0),(274365,0),(274366,0),(274367,0),(274368,0),(274369,0),(274370,0),(274371,0),(274372,0),(274373,0),(274374,0),(274375,0),(274376,0),(274377,0),(274378,0),(274379,0),(274380,0),(274381,0),(274382,0),(274383,0),(274384,0),(274385,0),(274386,0),(274387,0),(274388,0),(274389,0),(274390,0),(274391,0),(274392,0),(274393,0),(274394,0),(274395,0),(274396,0),(274397,0),(274398,0),(274399,0),(274400,0),(274401,0),(274402,0),(274403,0),(274404,0),(274405,0),(274406,0),(274407,0),(274408,0),(274409,0),(274410,0),(274411,0),(274412,0),(274413,0),(274414,0),(274415,0),(274416,0),(274417,0),(274418,0),(274419,0),(274420,0),(274421,0),(274422,0),(274423,0),(274424,0),(274425,0),(274426,0),(274427,0),(274428,0),(274429,0),(274430,0),(274431,0),(274432,0),(274433,0),(274434,0),(274435,0),(274436,0),(274437,0),(274438,0),(274439,0),(274440,0),(274441,0),(274442,0),(274443,0),(274444,0),(274445,0),(274446,0),(274447,0),(274448,0),(274449,0),(274450,0),(274451,0),(274452,0),(274453,0),(274454,0),(274455,0),(274456,0),(274457,0),(274458,0),(274459,0),(274460,0),(274461,0),(274462,0),(274463,0),(274464,0),(274465,0),(274466,0),(274467,0),(274468,0),(274469,0),(274470,0),(274471,0),(274472,0),(274473,0),(274474,0),(274475,0),(274476,0),(274477,0),(274478,0),(274479,0),(274480,0),(274481,0),(274482,0),(274483,0),(274484,0),(274485,0),(274486,0),(274487,0),(274488,0),(274489,0),(274490,0),(274491,0),(274492,0),(274493,0),(274494,0),(274495,0),(274496,0),(274497,0),(274498,0),(274499,0),(274500,0),(274501,0),(274502,0),(274503,0),(274504,0),(274505,0),(274506,0),(274507,0),(274508,0),(274509,0),(274510,0),(274511,0),(274512,0),(274513,0),(274514,0),(274515,0),(274516,0),(274517,0),(274518,0),(274519,0),(274520,0),(274521,0),(274522,0),(274523,0),(274524,0),(274525,0),(274526,0),(274527,0),(274528,0),(274529,0),(274530,0),(274531,0),(274532,0),(274533,0),(274534,0),(274535,0),(274536,0),(274537,0),(274538,0),(274539,0),(274540,0),(274541,0),(274542,0),(274543,0),(274544,0),(274545,0),(274546,0),(274547,0),(274548,0),(274549,0),(274550,0),(274551,0),(274552,0),(274553,0),(274554,0),(274555,0),(274556,0);
/*!40000 ALTER TABLE `pr_t_city_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_country`
--

DROP TABLE IF EXISTS `pr_t_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_country` (
  `pk_c_code` char(2) NOT NULL,
  `s_name` varchar(80) NOT NULL,
  `s_slug` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`pk_c_code`),
  KEY `idx_s_slug` (`s_slug`),
  KEY `idx_s_name` (`s_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_country`
--

LOCK TABLES `pr_t_country` WRITE;
/*!40000 ALTER TABLE `pr_t_country` DISABLE KEYS */;
INSERT INTO `pr_t_country` VALUES ('ID','Indonesia','indonesia');
/*!40000 ALTER TABLE `pr_t_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_country_stats`
--

DROP TABLE IF EXISTS `pr_t_country_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_country_stats` (
  `fk_c_country_code` char(2) NOT NULL,
  `i_num_items` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fk_c_country_code`),
  KEY `idx_num_items` (`i_num_items`),
  CONSTRAINT `pr_t_country_stats_ibfk_1` FOREIGN KEY (`fk_c_country_code`) REFERENCES `pr_t_country` (`pk_c_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_country_stats`
--

LOCK TABLES `pr_t_country_stats` WRITE;
/*!40000 ALTER TABLE `pr_t_country_stats` DISABLE KEYS */;
INSERT INTO `pr_t_country_stats` VALUES ('ID',0);
/*!40000 ALTER TABLE `pr_t_country_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_cron`
--

DROP TABLE IF EXISTS `pr_t_cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_cron` (
  `e_type` enum('INSTANT','HOURLY','DAILY','WEEKLY','CUSTOM') NOT NULL,
  `d_last_exec` datetime NOT NULL,
  `d_next_exec` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_cron`
--

LOCK TABLES `pr_t_cron` WRITE;
/*!40000 ALTER TABLE `pr_t_cron` DISABLE KEYS */;
INSERT INTO `pr_t_cron` VALUES ('HOURLY','2015-04-17 05:50:42','2015-04-17 06:50:42'),('DAILY','2015-04-17 04:49:23','2015-04-18 04:49:23'),('WEEKLY','2015-04-17 04:49:23','2015-04-24 04:49:23');
/*!40000 ALTER TABLE `pr_t_cron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_currency`
--

DROP TABLE IF EXISTS `pr_t_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_currency` (
  `pk_c_code` char(3) NOT NULL,
  `s_name` varchar(40) NOT NULL,
  `s_description` varchar(80) DEFAULT NULL,
  `b_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pk_c_code`),
  UNIQUE KEY `s_name` (`s_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_currency`
--

LOCK TABLES `pr_t_currency` WRITE;
/*!40000 ALTER TABLE `pr_t_currency` DISABLE KEYS */;
INSERT INTO `pr_t_currency` VALUES ('EUR','European Union euro','Euro €',1),('GBP','United Kingdom pound','Pound £',1),('IDR','Indonesia Rupiah','Rp.',1),('USD','United States dollar','Dollar US$',1);
/*!40000 ALTER TABLE `pr_t_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_item`
--

DROP TABLE IF EXISTS `pr_t_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_item` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_i_user_id` int(10) unsigned DEFAULT NULL,
  `fk_i_category_id` int(10) unsigned NOT NULL,
  `dt_pub_date` datetime NOT NULL,
  `dt_mod_date` datetime DEFAULT NULL,
  `f_price` float DEFAULT NULL,
  `i_price` bigint(20) DEFAULT NULL,
  `fk_c_currency_code` char(3) DEFAULT NULL,
  `s_contact_name` varchar(100) DEFAULT NULL,
  `s_contact_email` varchar(140) NOT NULL,
  `s_ip` varchar(64) NOT NULL DEFAULT '',
  `b_premium` tinyint(1) NOT NULL DEFAULT '0',
  `b_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `b_active` tinyint(1) NOT NULL DEFAULT '0',
  `b_spam` tinyint(1) NOT NULL DEFAULT '0',
  `s_secret` varchar(40) DEFAULT NULL,
  `b_show_email` tinyint(1) DEFAULT NULL,
  `dt_expiration` datetime NOT NULL DEFAULT '9999-12-31 23:59:59',
  PRIMARY KEY (`pk_i_id`),
  KEY `fk_i_user_id` (`fk_i_user_id`),
  KEY `idx_b_premium` (`b_premium`),
  KEY `idx_s_contact_email` (`s_contact_email`(10)),
  KEY `fk_i_category_id` (`fk_i_category_id`),
  KEY `fk_c_currency_code` (`fk_c_currency_code`),
  KEY `idx_pub_date` (`dt_pub_date`),
  KEY `idx_price` (`i_price`),
  CONSTRAINT `pr_t_item_ibfk_1` FOREIGN KEY (`fk_i_user_id`) REFERENCES `pr_t_user` (`pk_i_id`),
  CONSTRAINT `pr_t_item_ibfk_2` FOREIGN KEY (`fk_i_category_id`) REFERENCES `pr_t_category` (`pk_i_id`),
  CONSTRAINT `pr_t_item_ibfk_3` FOREIGN KEY (`fk_c_currency_code`) REFERENCES `pr_t_currency` (`pk_c_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_item`
--

LOCK TABLES `pr_t_item` WRITE;
/*!40000 ALTER TABLE `pr_t_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_item_comment`
--

DROP TABLE IF EXISTS `pr_t_item_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_item_comment` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `dt_pub_date` datetime NOT NULL,
  `s_title` varchar(200) NOT NULL,
  `s_author_name` varchar(100) NOT NULL,
  `s_author_email` varchar(100) NOT NULL,
  `s_body` text NOT NULL,
  `b_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `b_active` tinyint(1) NOT NULL DEFAULT '0',
  `b_spam` tinyint(1) NOT NULL DEFAULT '0',
  `fk_i_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`pk_i_id`),
  KEY `fk_i_item_id` (`fk_i_item_id`),
  KEY `fk_i_user_id` (`fk_i_user_id`),
  CONSTRAINT `pr_t_item_comment_ibfk_1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `pr_t_item` (`pk_i_id`),
  CONSTRAINT `pr_t_item_comment_ibfk_2` FOREIGN KEY (`fk_i_user_id`) REFERENCES `pr_t_user` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_item_comment`
--

LOCK TABLES `pr_t_item_comment` WRITE;
/*!40000 ALTER TABLE `pr_t_item_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_item_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_item_description`
--

DROP TABLE IF EXISTS `pr_t_item_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_item_description` (
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `fk_c_locale_code` char(5) NOT NULL,
  `s_title` varchar(100) NOT NULL,
  `s_description` mediumtext NOT NULL,
  PRIMARY KEY (`fk_i_item_id`,`fk_c_locale_code`),
  FULLTEXT KEY `s_description` (`s_description`,`s_title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_item_description`
--

LOCK TABLES `pr_t_item_description` WRITE;
/*!40000 ALTER TABLE `pr_t_item_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_item_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_item_location`
--

DROP TABLE IF EXISTS `pr_t_item_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_item_location` (
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `fk_c_country_code` char(2) DEFAULT NULL,
  `s_country` varchar(40) DEFAULT NULL,
  `s_address` varchar(100) DEFAULT NULL,
  `s_zip` varchar(15) DEFAULT NULL,
  `fk_i_region_id` int(10) unsigned DEFAULT NULL,
  `s_region` varchar(100) DEFAULT NULL,
  `fk_i_city_id` int(10) unsigned DEFAULT NULL,
  `s_city` varchar(100) DEFAULT NULL,
  `fk_i_city_area_id` int(10) unsigned DEFAULT NULL,
  `s_city_area` varchar(200) DEFAULT NULL,
  `d_coord_lat` decimal(10,6) DEFAULT NULL,
  `d_coord_long` decimal(10,6) DEFAULT NULL,
  PRIMARY KEY (`fk_i_item_id`),
  KEY `fk_c_country_code` (`fk_c_country_code`),
  KEY `fk_i_region_id` (`fk_i_region_id`),
  KEY `fk_i_city_id` (`fk_i_city_id`),
  KEY `fk_i_city_area_id` (`fk_i_city_area_id`),
  CONSTRAINT `pr_t_item_location_ibfk_1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `pr_t_item` (`pk_i_id`),
  CONSTRAINT `pr_t_item_location_ibfk_2` FOREIGN KEY (`fk_c_country_code`) REFERENCES `pr_t_country` (`pk_c_code`),
  CONSTRAINT `pr_t_item_location_ibfk_3` FOREIGN KEY (`fk_i_region_id`) REFERENCES `pr_t_region` (`pk_i_id`),
  CONSTRAINT `pr_t_item_location_ibfk_4` FOREIGN KEY (`fk_i_city_id`) REFERENCES `pr_t_city` (`pk_i_id`),
  CONSTRAINT `pr_t_item_location_ibfk_5` FOREIGN KEY (`fk_i_city_area_id`) REFERENCES `pr_t_city_area` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_item_location`
--

LOCK TABLES `pr_t_item_location` WRITE;
/*!40000 ALTER TABLE `pr_t_item_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_item_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_item_meta`
--

DROP TABLE IF EXISTS `pr_t_item_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_item_meta` (
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `fk_i_field_id` int(10) unsigned NOT NULL,
  `s_value` text,
  `s_multi` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`fk_i_item_id`,`fk_i_field_id`,`s_multi`),
  KEY `s_value` (`s_value`(255)),
  KEY `fk_i_field_id` (`fk_i_field_id`),
  CONSTRAINT `pr_t_item_meta_ibfk_1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `pr_t_item` (`pk_i_id`),
  CONSTRAINT `pr_t_item_meta_ibfk_2` FOREIGN KEY (`fk_i_field_id`) REFERENCES `pr_t_meta_fields` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_item_meta`
--

LOCK TABLES `pr_t_item_meta` WRITE;
/*!40000 ALTER TABLE `pr_t_item_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_item_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_item_resource`
--

DROP TABLE IF EXISTS `pr_t_item_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_item_resource` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `s_name` varchar(60) DEFAULT NULL,
  `s_extension` varchar(10) DEFAULT NULL,
  `s_content_type` varchar(40) DEFAULT NULL,
  `s_path` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`pk_i_id`),
  KEY `fk_i_item_id` (`fk_i_item_id`),
  KEY `idx_s_content_type` (`pk_i_id`,`s_content_type`(10)),
  CONSTRAINT `pr_t_item_resource_ibfk_1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `pr_t_item` (`pk_i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_item_resource`
--

LOCK TABLES `pr_t_item_resource` WRITE;
/*!40000 ALTER TABLE `pr_t_item_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_item_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_item_stats`
--

DROP TABLE IF EXISTS `pr_t_item_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_item_stats` (
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `i_num_views` int(10) unsigned NOT NULL DEFAULT '0',
  `i_num_spam` int(10) unsigned NOT NULL DEFAULT '0',
  `i_num_repeated` int(10) unsigned NOT NULL DEFAULT '0',
  `i_num_bad_classified` int(10) unsigned NOT NULL DEFAULT '0',
  `i_num_offensive` int(10) unsigned NOT NULL DEFAULT '0',
  `i_num_expired` int(10) unsigned NOT NULL DEFAULT '0',
  `i_num_premium_views` int(10) unsigned NOT NULL DEFAULT '0',
  `dt_date` date NOT NULL,
  PRIMARY KEY (`fk_i_item_id`,`dt_date`),
  KEY `dt_date` (`dt_date`,`fk_i_item_id`),
  CONSTRAINT `pr_t_item_stats_ibfk_1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `pr_t_item` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_item_stats`
--

LOCK TABLES `pr_t_item_stats` WRITE;
/*!40000 ALTER TABLE `pr_t_item_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_item_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_keywords`
--

DROP TABLE IF EXISTS `pr_t_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_keywords` (
  `s_md5` varchar(32) NOT NULL,
  `fk_c_locale_code` char(5) NOT NULL,
  `s_original_text` varchar(255) NOT NULL,
  `s_anchor_text` varchar(255) NOT NULL,
  `s_normalized_text` varchar(255) NOT NULL,
  `fk_i_category_id` int(10) unsigned DEFAULT NULL,
  `fk_i_city_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`s_md5`,`fk_c_locale_code`),
  KEY `fk_i_category_id` (`fk_i_category_id`),
  KEY `fk_i_city_id` (`fk_i_city_id`),
  KEY `fk_c_locale_code` (`fk_c_locale_code`),
  CONSTRAINT `pr_t_keywords_ibfk_1` FOREIGN KEY (`fk_i_category_id`) REFERENCES `pr_t_category` (`pk_i_id`),
  CONSTRAINT `pr_t_keywords_ibfk_2` FOREIGN KEY (`fk_i_city_id`) REFERENCES `pr_t_city` (`pk_i_id`),
  CONSTRAINT `pr_t_keywords_ibfk_3` FOREIGN KEY (`fk_c_locale_code`) REFERENCES `pr_t_locale` (`pk_c_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_keywords`
--

LOCK TABLES `pr_t_keywords` WRITE;
/*!40000 ALTER TABLE `pr_t_keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_latest_searches`
--

DROP TABLE IF EXISTS `pr_t_latest_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_latest_searches` (
  `d_date` datetime NOT NULL,
  `s_search` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_latest_searches`
--

LOCK TABLES `pr_t_latest_searches` WRITE;
/*!40000 ALTER TABLE `pr_t_latest_searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_latest_searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_locale`
--

DROP TABLE IF EXISTS `pr_t_locale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_locale` (
  `pk_c_code` char(5) NOT NULL,
  `s_name` varchar(100) NOT NULL,
  `s_short_name` varchar(40) NOT NULL,
  `s_description` varchar(100) NOT NULL,
  `s_version` varchar(20) NOT NULL,
  `s_author_name` varchar(100) NOT NULL,
  `s_author_url` varchar(100) NOT NULL,
  `s_currency_format` varchar(50) NOT NULL,
  `s_dec_point` varchar(2) DEFAULT '.',
  `s_thousands_sep` varchar(2) DEFAULT '',
  `i_num_dec` tinyint(4) DEFAULT '2',
  `s_date_format` varchar(20) NOT NULL,
  `s_stop_words` text,
  `b_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `b_enabled_bo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pk_c_code`),
  UNIQUE KEY `s_short_name` (`s_short_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_locale`
--

LOCK TABLES `pr_t_locale` WRITE;
/*!40000 ALTER TABLE `pr_t_locale` DISABLE KEYS */;
INSERT INTO `pr_t_locale` VALUES ('en_US','English (US)','English','American english translation','2.3','Osclass','http://osclass.org/','{NUMBER} {CURRENCY}','.','',2,'m/d/Y','i,a,about,an,are,as,at,be,by,com,for,from,how,in,is,it,of,on,or,that,the,this,to,was,what,when,where,who,will,with,the',1,1),('id_ID','Indonesian','Indonesian','Indonesian translation','3.1.0','Osclass','http://osclass.org/','{CURRENCY} {NUMBER}','.','.',0,'m/d/Y','',1,1);
/*!40000 ALTER TABLE `pr_t_locale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_locations_tmp`
--

DROP TABLE IF EXISTS `pr_t_locations_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_locations_tmp` (
  `id_location` varchar(10) NOT NULL,
  `e_type` enum('COUNTRY','REGION','CITY') NOT NULL,
  PRIMARY KEY (`id_location`,`e_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_locations_tmp`
--

LOCK TABLES `pr_t_locations_tmp` WRITE;
/*!40000 ALTER TABLE `pr_t_locations_tmp` DISABLE KEYS */;
INSERT INTO `pr_t_locations_tmp` VALUES ('274556','CITY'),('274557','CITY'),('274558','CITY'),('274559','CITY'),('274560','CITY'),('274561','CITY'),('274562','CITY'),('274563','CITY'),('274564','CITY'),('274565','CITY'),('274566','CITY'),('274567','CITY'),('274568','CITY'),('274569','CITY'),('274570','CITY'),('274571','CITY'),('274572','CITY'),('274573','CITY'),('274574','CITY'),('274575','CITY'),('274576','CITY'),('274577','CITY'),('274578','CITY'),('274579','CITY'),('274580','CITY'),('274581','CITY'),('274582','CITY'),('274583','CITY'),('274584','CITY'),('274585','CITY'),('274586','CITY'),('274587','CITY'),('274588','CITY'),('274589','CITY'),('274590','CITY'),('274591','CITY'),('274592','CITY'),('274593','CITY'),('274594','CITY'),('274595','CITY'),('274596','CITY'),('274597','CITY'),('274598','CITY'),('274599','CITY'),('274600','CITY'),('274601','CITY'),('274602','CITY'),('274603','CITY'),('274604','CITY'),('274605','CITY'),('274606','CITY'),('274607','CITY'),('274608','CITY'),('274609','CITY'),('274610','CITY'),('274611','CITY'),('274612','CITY'),('274613','CITY'),('274614','CITY'),('274615','CITY'),('274616','CITY'),('274617','CITY'),('274618','CITY'),('274619','CITY'),('274620','CITY'),('274621','CITY'),('274622','CITY'),('274623','CITY'),('274624','CITY'),('274625','CITY'),('274626','CITY'),('274627','CITY'),('274628','CITY'),('274629','CITY'),('274630','CITY'),('781431','REGION'),('781432','REGION'),('781433','REGION'),('781434','REGION'),('781435','REGION'),('781436','REGION'),('781437','REGION'),('781438','REGION'),('781439','REGION'),('781440','REGION'),('781441','REGION'),('781442','REGION'),('781443','REGION'),('781444','REGION'),('781445','REGION'),('781446','REGION'),('781447','REGION'),('781448','REGION'),('781449','REGION'),('781450','REGION'),('781451','REGION'),('781452','REGION'),('781453','REGION'),('781454','REGION'),('781455','REGION'),('781456','REGION'),('781457','REGION'),('781458','REGION'),('781459','REGION'),('781460','REGION'),('781461','REGION'),('781462','REGION'),('781463','REGION'),('ID','COUNTRY');
/*!40000 ALTER TABLE `pr_t_locations_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_log`
--

DROP TABLE IF EXISTS `pr_t_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_log` (
  `dt_date` datetime NOT NULL,
  `s_section` varchar(50) NOT NULL,
  `s_action` varchar(50) NOT NULL,
  `fk_i_id` int(10) unsigned NOT NULL,
  `s_data` varchar(250) NOT NULL,
  `s_ip` varchar(50) NOT NULL,
  `s_who` varchar(50) NOT NULL,
  `fk_i_who_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_log`
--

LOCK TABLES `pr_t_log` WRITE;
/*!40000 ALTER TABLE `pr_t_log` DISABLE KEYS */;
INSERT INTO `pr_t_log` VALUES ('2015-04-17 04:33:26','item','add',1,'Example Ad','::1','admin',0),('2015-04-17 04:56:30','item','add',2,'fd','::1','user',0),('2015-04-17 04:58:09','itemActions','deleteResourcesFromHD',1,'1','::1','admin',1),('2015-04-17 04:58:09','itemActions','deleteResourcesFromHD',1,'0','::1','admin',1),('2015-04-17 04:58:09','item','delete',1,'Example Ad','::1','admin',1),('2015-04-17 04:58:10','Item','deleteResourcesFromHD',1,'1','::1','admin',1),('2015-04-17 04:58:10','Item','deleteResourcesFromHD',1,'0','::1','admin',1),('2015-04-17 04:58:14','itemActions','deleteResourcesFromHD',2,'2','::1','admin',1),('2015-04-17 04:58:14','item','delete resource',1,'1','::1','admin',1),('2015-04-17 04:58:14','item','delete resource backtrace',1,'#0 osc_deleteResource called@ [C:\\xampp\\htdocs\\pasarraya\\oc-includes\\osclass\\ItemActions.php:810] / #1 deleteResourcesFromHD called@ [C:\\xampp\\htdocs\\pasarraya\\oc-includes\\osclass\\ItemActions.php:788] / #2 delete called@ [C:\\xampp\\htdocs\\pasarraya\\oc','::1','admin',1),('2015-04-17 04:58:14','itemActions','deleteResourcesFromHD',2,'1,','::1','admin',1),('2015-04-17 04:58:14','item','delete',2,'fd','::1','admin',1),('2015-04-17 04:58:14','Item','deleteResourcesFromHD',2,'2','::1','admin',1),('2015-04-17 04:58:15','item','delete resource',1,'1','::1','admin',1),('2015-04-17 04:58:15','item','delete resource backtrace',1,'#0 osc_deleteResource called@ [C:\\xampp\\htdocs\\pasarraya\\oc-includes\\osclass\\model\\Item.php:990] / #1 deleteResourcesFromHD called@ [C:\\xampp\\htdocs\\pasarraya\\oc-includes\\osclass\\model\\Item.php:961] / #2 deleteByPrimaryKey called@ [C:\\xampp\\htdocs\\pa','::1','admin',1),('2015-04-17 04:58:15','Item','deleteResourcesFromHD',2,'1,','::1','admin',1),('2015-04-17 04:58:50','item','add',3,'jkjk','::1','user',0),('2015-04-17 04:59:03','itemActions','deleteResourcesFromHD',3,'3','::1','admin',1),('2015-04-17 04:59:03','itemActions','deleteResourcesFromHD',3,'0','::1','admin',1),('2015-04-17 04:59:03','item','delete',3,'jkjk','::1','admin',1),('2015-04-17 04:59:03','Item','deleteResourcesFromHD',3,'3','::1','admin',1),('2015-04-17 04:59:03','Item','deleteResourcesFromHD',3,'0','::1','admin',1),('2015-04-17 05:03:38','item','add',4,'frde','::1','user',0),('2015-04-17 05:32:18','itemActions','deleteResourcesFromHD',4,'4','::1','admin',1),('2015-04-17 05:32:18','itemActions','deleteResourcesFromHD',4,'0','::1','admin',1),('2015-04-17 05:32:18','item','delete',4,' in rew','::1','admin',1),('2015-04-17 05:32:18','Item','deleteResourcesFromHD',4,'4','::1','admin',1),('2015-04-17 05:32:18','Item','deleteResourcesFromHD',4,'0','::1','admin',1);
/*!40000 ALTER TABLE `pr_t_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_meta_categories`
--

DROP TABLE IF EXISTS `pr_t_meta_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_meta_categories` (
  `fk_i_category_id` int(10) unsigned NOT NULL,
  `fk_i_field_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`fk_i_category_id`,`fk_i_field_id`),
  KEY `fk_i_field_id` (`fk_i_field_id`),
  CONSTRAINT `pr_t_meta_categories_ibfk_1` FOREIGN KEY (`fk_i_category_id`) REFERENCES `pr_t_category` (`pk_i_id`),
  CONSTRAINT `pr_t_meta_categories_ibfk_2` FOREIGN KEY (`fk_i_field_id`) REFERENCES `pr_t_meta_fields` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_meta_categories`
--

LOCK TABLES `pr_t_meta_categories` WRITE;
/*!40000 ALTER TABLE `pr_t_meta_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_meta_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_meta_fields`
--

DROP TABLE IF EXISTS `pr_t_meta_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_meta_fields` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varchar(255) NOT NULL,
  `s_slug` varchar(255) NOT NULL,
  `e_type` enum('TEXT','TEXTAREA','DROPDOWN','RADIO','CHECKBOX','URL','DATE','DATEINTERVAL') NOT NULL DEFAULT 'TEXT',
  `s_options` varchar(2048) DEFAULT NULL,
  `b_required` tinyint(1) NOT NULL DEFAULT '0',
  `b_searchable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_meta_fields`
--

LOCK TABLES `pr_t_meta_fields` WRITE;
/*!40000 ALTER TABLE `pr_t_meta_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_meta_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_pages`
--

DROP TABLE IF EXISTS `pr_t_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_pages` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_internal_name` varchar(50) DEFAULT NULL,
  `b_indelible` tinyint(1) NOT NULL DEFAULT '0',
  `b_link` tinyint(1) NOT NULL DEFAULT '1',
  `dt_pub_date` datetime NOT NULL,
  `dt_mod_date` datetime DEFAULT NULL,
  `i_order` int(3) NOT NULL DEFAULT '0',
  `s_meta` text,
  PRIMARY KEY (`pk_i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_pages`
--

LOCK TABLES `pr_t_pages` WRITE;
/*!40000 ALTER TABLE `pr_t_pages` DISABLE KEYS */;
INSERT INTO `pr_t_pages` VALUES (1,'email_item_inquiry',1,1,'2015-04-17 09:33:16',NULL,0,NULL),(2,'email_user_validation',1,1,'2015-04-17 09:33:16',NULL,0,NULL),(3,'email_user_registration',1,1,'2015-04-17 09:33:16',NULL,0,NULL),(4,'email_send_friend',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(5,'alert_email_hourly',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(6,'alert_email_daily',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(7,'alert_email_weekly',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(8,'alert_email_instant',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(9,'email_new_comment_admin',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(10,'email_new_item_non_register_user',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(11,'email_item_validation',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(12,'email_admin_new_item',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(13,'email_user_forgot_password',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(14,'email_new_email',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(15,'email_alert_validation',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(16,'email_comment_validated',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(17,'email_item_validation_non_register_user',1,1,'2015-04-17 09:33:17',NULL,0,NULL),(18,'email_admin_new_user',1,1,'2015-04-17 09:33:18',NULL,0,NULL),(19,'email_contact_user',1,1,'2015-04-17 09:33:18',NULL,0,NULL),(20,'email_new_comment_user',1,1,'2015-04-17 09:33:18',NULL,0,NULL),(21,'email_new_admin',1,1,'2015-04-17 09:33:18',NULL,0,NULL),(22,'email_warn_expiration',1,1,'2015-04-17 09:33:18',NULL,0,NULL),(23,'example_page',0,0,'2015-04-17 04:33:26','2015-04-17 04:33:26',1,'\"\"');
/*!40000 ALTER TABLE `pr_t_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_pages_description`
--

DROP TABLE IF EXISTS `pr_t_pages_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_pages_description` (
  `fk_i_pages_id` int(10) unsigned NOT NULL,
  `fk_c_locale_code` char(5) NOT NULL,
  `s_title` varchar(255) NOT NULL,
  `s_text` text,
  PRIMARY KEY (`fk_i_pages_id`,`fk_c_locale_code`),
  KEY `fk_c_locale_code` (`fk_c_locale_code`),
  CONSTRAINT `pr_t_pages_description_ibfk_1` FOREIGN KEY (`fk_i_pages_id`) REFERENCES `pr_t_pages` (`pk_i_id`),
  CONSTRAINT `pr_t_pages_description_ibfk_2` FOREIGN KEY (`fk_c_locale_code`) REFERENCES `pr_t_locale` (`pk_c_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_pages_description`
--

LOCK TABLES `pr_t_pages_description` WRITE;
/*!40000 ALTER TABLE `pr_t_pages_description` DISABLE KEYS */;
INSERT INTO `pr_t_pages_description` VALUES (1,'en_US','{WEB_TITLE} - Someone has a question about your listing','<p>Hi {CONTACT_NAME}!</p><p>{USER_NAME} ({USER_EMAIL}, {USER_PHONE}) left you a message about your listing <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>:</p><p>{COMMENT}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(1,'id_ID','{WEB_TITLE} - Someone has a question about your listing','<p>Hi {CONTACT_NAME}!</p><p>{USER_NAME} ({USER_EMAIL}, {USER_PHONE}) left you a message about your listing <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>:</p><p>{COMMENT}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(2,'en_US','Please validate your {WEB_TITLE} account','<p>Hi {USER_NAME},</p><p>Please validate your registration by clicking on the following link: {VALIDATION_LINK}</p><p>Thank you!</p><p>Regards,</p><p>{WEB_LINK}</p>'),(2,'id_ID','Please validate your {WEB_TITLE} account','<p>Hi {USER_NAME},</p><p>Please validate your registration by clicking on the following link: {VALIDATION_LINK}</p><p>Thank you!</p><p>Regards,</p><p>{WEB_LINK}</p>'),(3,'en_US','{WEB_TITLE} - Registration successful!','<p>Hi {USER_NAME},</p><p>You\'ve successfully registered for {WEB_LINK}.</p><p>Thank you!</p><p>Regards,</p><p>{WEB_LINK}</p>'),(3,'id_ID','{WEB_TITLE} - Registration successful!','<p>Hi {USER_NAME},</p><p>You\'ve successfully registered for {WEB_LINK}.</p><p>Thank you!</p><p>Regards,</p><p>{WEB_LINK}</p>'),(4,'en_US','Look at what I discovered on {WEB_TITLE}','<p>Hi {FRIEND_NAME},</p><p>Your friend {USER_NAME} wants to share this listing with you <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>.</p><p>Message:</p><p>{COMMENT}</p><p>Regards,</p><p>{WEB_TITLE}</p>'),(4,'id_ID','Look at what I discovered on {WEB_TITLE}','<p>Hi {FRIEND_NAME},</p><p>Your friend {USER_NAME} wants to share this listing with you <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>.</p><p>Message:</p><p>{COMMENT}</p><p>Regards,</p><p>{WEB_TITLE}</p>'),(5,'en_US','{WEB_TITLE} - New listings in the last hour','<p>Hi {USER_NAME},</p><p>New listings have been published in the last hour. Take a look at them:</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(5,'id_ID','{WEB_TITLE} - New listings in the last hour','<p>Hi {USER_NAME},</p><p>New listings have been published in the last hour. Take a look at them:</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(6,'en_US','{WEB_TITLE} - New listings in the last day','<p>Hi {USER_NAME},</p><p>New listings have been published in the last day. Take a look at them:</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(6,'id_ID','{WEB_TITLE} - New listings in the last day','<p>Hi {USER_NAME},</p><p>New listings have been published in the last day. Take a look at them:</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(7,'en_US','{WEB_TITLE} - New listings in the last week','<p>Hi {USER_NAME},</p><p>New listings have been published in the last week. Take a look at them:</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(7,'id_ID','{WEB_TITLE} - New listings in the last week','<p>Hi {USER_NAME},</p><p>New listings have been published in the last week. Take a look at them:</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(8,'en_US','{WEB_TITLE} - New listings','<p>Hi {USER_NAME},</p><p>A new listing has been published, check it out!</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(8,'id_ID','{WEB_TITLE} - New listings','<p>Hi {USER_NAME},</p><p>A new listing has been published, check it out!</p><p>{ADS}</p><p>-------------</p><p>To unsubscribe from this alert, click on: {UNSUB_LINK}</p><p>{WEB_LINK}</p>'),(9,'en_US','{WEB_TITLE} - New comment','<p>Someone commented on the listing <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>.</p><p>Commenter: {COMMENT_AUTHOR}<br />Commenter\'s email: {COMMENT_EMAIL}<br />Title: {COMMENT_TITLE}<br />Comment: {COMMENT_TEXT}</p>'),(9,'id_ID','{WEB_TITLE} - New comment','<p>Someone commented on the listing <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>.</p><p>Commenter: {COMMENT_AUTHOR}<br />Commenter\'s email: {COMMENT_EMAIL}<br />Title: {COMMENT_TITLE}<br />Comment: {COMMENT_TEXT}</p>'),(10,'en_US','{WEB_TITLE} - Edit options for the listing {ITEM_TITLE}','<p>Hi {USER_NAME},</p><p>You\'re not registered at {WEB_LINK}, but you can still edit or delete the listing <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a> for a short period of time.</p><p>You can edit your listing by following this link: {EDIT_LINK}</p><p>You can delete your listing by following this link: {DELETE_LINK}</p><p>If you register as a user, you will have full access to editing options.</p><p>Regards,</p><p>{WEB_LINK}</p>'),(10,'id_ID','{WEB_TITLE} - Edit options for the listing {ITEM_TITLE}','<p>Hi {USER_NAME},</p><p>You\'re not registered at {WEB_LINK}, but you can still edit or delete the listing <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a> for a short period of time.</p><p>You can edit your listing by following this link: {EDIT_LINK}</p><p>You can delete your listing by following this link: {DELETE_LINK}</p><p>If you register as a user, you will have full access to editing options.</p><p>Regards,</p><p>{WEB_LINK}</p>'),(11,'en_US','{WEB_TITLE} - Validate your listing','<p>Hi {USER_NAME},</p><p>You\'re receiving this e-mail because a listing has been published at {WEB_LINK}. Please validate this listing by clicking on the following link: {VALIDATION_LINK}. If you didn\'t publish this listing, please ignore this email.</p><p>Listing details:</p><p>Contact name: {USER_NAME}<br />Contact e-mail: {USER_EMAIL}</p><p>{ITEM_DESCRIPTION}</p><p>Url: {ITEM_URL}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(11,'id_ID','{WEB_TITLE} - Validate your listing','<p>Hi {USER_NAME},</p><p>You\'re receiving this e-mail because a listing has been published at {WEB_LINK}. Please validate this listing by clicking on the following link: {VALIDATION_LINK}. If you didn\'t publish this listing, please ignore this email.</p><p>Listing details:</p><p>Contact name: {USER_NAME}<br />Contact e-mail: {USER_EMAIL}</p><p>{ITEM_DESCRIPTION}</p><p>Url: {ITEM_URL}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(12,'en_US','{WEB_TITLE} - A new listing has been published','<p>Dear {WEB_TITLE} admin,</p><p>You\'re receiving this email because a listing has been published at {WEB_LINK}.</p><p>Listing details:</p><p>Contact name: {USER_NAME}<br />Contact email: {USER_EMAIL}</p><p>{ITEM_DESCRIPTION}</p><p>Url: {ITEM_URL}</p><p>You can edit this listing by clicking on the following link: {EDIT_LINK}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(12,'id_ID','{WEB_TITLE} - A new listing has been published','<p>Dear {WEB_TITLE} admin,</p><p>You\'re receiving this email because a listing has been published at {WEB_LINK}.</p><p>Listing details:</p><p>Contact name: {USER_NAME}<br />Contact email: {USER_EMAIL}</p><p>{ITEM_DESCRIPTION}</p><p>Url: {ITEM_URL}</p><p>You can edit this listing by clicking on the following link: {EDIT_LINK}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(13,'en_US','{WEB_TITLE} - Recover your password','<p>Hi {USER_NAME},</p><p>We\'ve sent you this e-mail because you\'ve requested a password reminder. Follow this link to recover it: {PASSWORD_LINK}</p><p>The link will be deactivated in 24 hours.</p><p>If you didn\'t request a password reminder, please ignore this message. This request was made from IP {IP_ADDRESS} on {DATE_TIME}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(13,'id_ID','{WEB_TITLE} - Recover your password','<p>Hi {USER_NAME},</p><p>We\'ve sent you this e-mail because you\'ve requested a password reminder. Follow this link to recover it: {PASSWORD_LINK}</p><p>The link will be deactivated in 24 hours.</p><p>If you didn\'t request a password reminder, please ignore this message. This request was made from IP {IP_ADDRESS} on {DATE_TIME}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(14,'en_US','{WEB_TITLE} - You requested an email change','<p>Hi {USER_NAME}</p><p>You\'re receiving this e-mail because you requested an e-mail change. Please confirm this new e-mail address by clicking on the following validation link: {VALIDATION_LINK}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(14,'id_ID','{WEB_TITLE} - You requested an email change','<p>Hi {USER_NAME}</p><p>You\'re receiving this e-mail because you requested an e-mail change. Please confirm this new e-mail address by clicking on the following validation link: {VALIDATION_LINK}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(15,'en_US','{WEB_TITLE} - Please validate your alert','<p>Hi {USER_NAME},</p><p>Please validate your alert registration by clicking on the following link: {VALIDATION_LINK}</p><p>Thank you!</p><p>Regards,</p><p>{WEB_LINK}</p>'),(15,'id_ID','{WEB_TITLE} - Please validate your alert','<p>Hi {USER_NAME},</p><p>Please validate your alert registration by clicking on the following link: {VALIDATION_LINK}</p><p>Thank you!</p><p>Regards,</p><p>{WEB_LINK}</p>'),(16,'en_US','{WEB_TITLE} - Your comment has been approved','<p>Hi {COMMENT_AUTHOR},</p><p>Your comment on <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a> has been approved.</p><p>Regards,</p><p>{WEB_LINK}</p>'),(16,'id_ID','{WEB_TITLE} - Your comment has been approved','<p>Hi {COMMENT_AUTHOR},</p><p>Your comment on <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a> has been approved.</p><p>Regards,</p><p>{WEB_LINK}</p>'),(17,'en_US','{WEB_TITLE} - Validate your listing','<p>Hi {USER_NAME},</p><p>You\'re receiving this e-mail because you’ve published a listing at {WEB_LINK}. Please validate this listing by clicking on the following link: {VALIDATION_LINK}. If you didn\'t publish this listing, please ignore this e-mail.</p><p>Listing details:</p><p>Contact name: {USER_NAME}<br />Contact e-mail: {USER_EMAIL}</p><p>{ITEM_DESCRIPTION}</p><p>Url: {ITEM_URL}</p><p>Even if you\'re not registered at {WEB_LINK}, you can still edit or delete your listing:</p><p>You can edit your listing by following this link: {EDIT_LINK}</p><p>You can delete your listing by following this link: {DELETE_LINK}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(17,'id_ID','{WEB_TITLE} - Validate your listing','<p>Hi {USER_NAME},</p><p>You\'re receiving this e-mail because you’ve published a listing at {WEB_LINK}. Please validate this listing by clicking on the following link: {VALIDATION_LINK}. If you didn\'t publish this listing, please ignore this e-mail.</p><p>Listing details:</p><p>Contact name: {USER_NAME}<br />Contact e-mail: {USER_EMAIL}</p><p>{ITEM_DESCRIPTION}</p><p>Url: {ITEM_URL}</p><p>Even if you\'re not registered at {WEB_LINK}, you can still edit or delete your listing:</p><p>You can edit your listing by following this link: {EDIT_LINK}</p><p>You can delete your listing by following this link: {DELETE_LINK}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(18,'en_US','{WEB_TITLE} - A new user has registered','<p>Dear {WEB_TITLE} admin,</p><p>You\'re receiving this email because a new user has been created at {WEB_LINK}.</p><p>User details:</p><p>Name: {USER_NAME}<br />E-mail: {USER_EMAIL}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(18,'id_ID','{WEB_TITLE} - A new user has registered','<p>Dear {WEB_TITLE} admin,</p><p>You\'re receiving this email because a new user has been created at {WEB_LINK}.</p><p>User details:</p><p>Name: {USER_NAME}<br />E-mail: {USER_EMAIL}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(19,'en_US','{WEB_TITLE} - Someone has a question for you','<p>Hi {CONTACT_NAME}!</p><p>{USER_NAME} ({USER_EMAIL}, {USER_PHONE}) left you a message:</p><p>{COMMENT}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(19,'id_ID','{WEB_TITLE} - Someone has a question for you','<p>Hi {CONTACT_NAME}!</p><p>{USER_NAME} ({USER_EMAIL}, {USER_PHONE}) left you a message:</p><p>{COMMENT}</p><p>Regards,</p><p>{WEB_LINK}</p>'),(20,'en_US','{WEB_TITLE} - Someone has commented on your listing','<p>There\'s a new comment on the listing: <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>.</p><p>Author: {COMMENT_AUTHOR}<br />Author\'s email: {COMMENT_EMAIL}<br />Title: {COMMENT_TITLE}<br />Comment: {COMMENT_TEXT}</p><p>{WEB_LINK}</p>'),(20,'id_ID','{WEB_TITLE} - Someone has commented on your listing','<p>There\'s a new comment on the listing: <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a>.</p><p>Author: {COMMENT_AUTHOR}<br />Author\'s email: {COMMENT_EMAIL}<br />Title: {COMMENT_TITLE}<br />Comment: {COMMENT_TEXT}</p><p>{WEB_LINK}</p>'),(21,'en_US','{WEB_TITLE} - Success creating admin account!','<p>Hi {ADMIN_NAME},</p><p>The admin of {WEB_LINK} has created an account for you,</p><ul><li>Username: {USERNAME}</li><li>Password: {PASSWORD}</li></ul><p>You can access the admin panel here {WEB_ADMIN_LINK}.</p><p>Thank you!</p><p>Regards,</p>'),(22,'en_US','{WEB_TITLE} - Your ad is about to expire','<p>Hi {USER_NAME},</p><p>Your listing <a href=\"{ITEM_URL}\">{ITEM_TITLE}</a> is about to expire at {WEB_LINK}.'),(23,'en_US','Example page title','This is an example page description. This is a good place to put your Terms of Service or any other help information.');
/*!40000 ALTER TABLE `pr_t_pages_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_plugin_category`
--

DROP TABLE IF EXISTS `pr_t_plugin_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_plugin_category` (
  `s_plugin_name` varchar(40) NOT NULL,
  `fk_i_category_id` int(10) unsigned NOT NULL,
  KEY `fk_i_category_id` (`fk_i_category_id`),
  CONSTRAINT `pr_t_plugin_category_ibfk_1` FOREIGN KEY (`fk_i_category_id`) REFERENCES `pr_t_category` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_plugin_category`
--

LOCK TABLES `pr_t_plugin_category` WRITE;
/*!40000 ALTER TABLE `pr_t_plugin_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_plugin_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_preference`
--

DROP TABLE IF EXISTS `pr_t_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_preference` (
  `s_section` varchar(40) NOT NULL,
  `s_name` varchar(40) NOT NULL,
  `s_value` longtext NOT NULL,
  `e_type` enum('STRING','INTEGER','BOOLEAN') NOT NULL,
  UNIQUE KEY `s_section` (`s_section`,`s_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_preference`
--

LOCK TABLES `pr_t_preference` WRITE;
/*!40000 ALTER TABLE `pr_t_preference` DISABLE KEYS */;
INSERT INTO `pr_t_preference` VALUES ('bender_theme','defaultShowAs@all','list','STRING'),('bender_theme','donation','0','STRING'),('bender_theme','footer_link','1','STRING'),('bender_theme','keyword_placeholder','ie. PHP Programmer','STRING'),('bender_theme','version','1.0.1','STRING'),('osclass','active_plugins','','STRING'),('osclass','admin_language','en_US','STRING'),('osclass','admin_theme','modern','STRING'),('osclass','akismetKey','','STRING'),('osclass','allowedExt','png,gif,jpg,jpeg','STRING'),('osclass','allow_report_osclass','1','BOOLEAN'),('osclass','auto_cron','1','STRING'),('osclass','auto_update','disabled','STRING'),('osclass','comments_per_page','10','INTEGER'),('osclass','contactEmail','admin@pasarraya.com','STRING'),('osclass','contact_attachment','0','STRING'),('osclass','csrf_name','CSRF236004688','STRING'),('osclass','currency','IDR','STRING'),('osclass','dateFormat','F j, Y','STRING'),('osclass','defaultOrderField@search','dt_pub_date','STRING'),('osclass','defaultOrderType@search','1','BOOLEAN'),('osclass','defaultResultsPerPage@search','12','STRING'),('osclass','defaultShowAs@search','list','STRING'),('osclass','description_character_length','5000','INTEGER'),('osclass','dimNormal','640x480','STRING'),('osclass','dimPreview','480x340','STRING'),('osclass','dimThumbnail','240x200','STRING'),('osclass','enabled_comments','1','BOOLEAN'),('osclass','enabled_recaptcha_items','0','BOOLEAN'),('osclass','enabled_users','1','BOOLEAN'),('osclass','enabled_user_registration','1','BOOLEAN'),('osclass','enabled_user_validation','1','BOOLEAN'),('osclass','enableField#f_price@items','1','BOOLEAN'),('osclass','enableField#images@items','1','BOOLEAN'),('osclass','force_aspect_image','0','BOOLEAN'),('osclass','installed_plugins','','STRING'),('osclass','items_wait_time','0','INTEGER'),('osclass','item_attachment','0','BOOLEAN'),('osclass','keep_original_image','1','BOOLEAN'),('osclass','language','id_ID','STRING'),('osclass','languages_downloaded','[\"en_US\",\"id_ID\"]','STRING'),('osclass','languages_last_version_check','1429240530','STRING'),('osclass','languages_to_update','[]','STRING'),('osclass','languages_update_count','0','STRING'),('osclass','last_version_check','1429238965','STRING'),('osclass','location_todo','500','STRING'),('osclass','logged_user_item_validation','1','BOOLEAN'),('osclass','mailserver_auth','','BOOLEAN'),('osclass','mailserver_host','localhost','STRING'),('osclass','mailserver_mail_from','','STRING'),('osclass','mailserver_name_from','','STRING'),('osclass','mailserver_password','','STRING'),('osclass','mailserver_pop','','BOOLEAN'),('osclass','mailserver_port','','INTEGER'),('osclass','mailserver_ssl','','STRING'),('osclass','mailserver_type','custom','STRING'),('osclass','mailserver_username','','STRING'),('osclass','marketAllowExternalSources','0','STRING'),('osclass','marketAPIConnect','','STRING'),('osclass','marketCategories','{\"languages\":{\"value\":\"98\",\"label\":\"Languages\",\"categories\":[]},\"plugins\":{\"value\":\"97\",\"label\":\"Plugins\",\"categories\":[{\"value\":\"103\",\"label\":\"Ad management\",\"categories\":[]},{\"value\":\"132\",\"label\":\"Appearance\",\"categories\":[]},{\"value\":\"105\",\"label\":\"Analytics\",\"categories\":[]},{\"value\":\"130\",\"label\":\"Attributes\",\"categories\":[]},{\"value\":\"106\",\"label\":\"Counters\",\"categories\":[]},{\"value\":\"107\",\"label\":\"Cloud tools\",\"categories\":[]},{\"value\":\"112\",\"label\":\"E-mail\",\"categories\":[]},{\"value\":\"115\",\"label\":\"Maps\",\"categories\":[]},{\"value\":\"116\",\"label\":\"Mobile\",\"categories\":[]},{\"value\":\"117\",\"label\":\"Messaging\",\"categories\":[]},{\"value\":\"119\",\"label\":\"Reviews &amp; Ratings\",\"categories\":[]},{\"value\":\"134\",\"label\":\"Search tools\",\"categories\":[]},{\"value\":\"121\",\"label\":\"User Management\",\"categories\":[]},{\"value\":\"122\",\"label\":\"Payments\",\"categories\":[]},{\"value\":\"124\",\"label\":\"Text processing \\/ WYSIWYG Editors\",\"categories\":[]},{\"value\":\"125\",\"label\":\"Video \\/ Audio\",\"categories\":[]},{\"value\":\"126\",\"label\":\"Social Networks\",\"categories\":[]},{\"value\":\"127\",\"label\":\"SEO\",\"categories\":[]},{\"value\":\"133\",\"label\":\"Security\",\"categories\":[]},{\"value\":\"128\",\"label\":\"Miscellaneous\",\"categories\":[]}]},\"themes\":{\"value\":\"96\",\"label\":\"Themes\",\"categories\":[{\"value\":\"102\",\"label\":\"General\",\"categories\":[]},{\"value\":\"100\",\"label\":\"Jobs\",\"categories\":[]},{\"value\":\"101\",\"label\":\"Real estate\",\"categories\":[]}]}}','STRING'),('osclass','marketDataUpdate','1429238999','STRING'),('osclass','marketURL','http://market.osclass.org/api/v2/','STRING'),('osclass','maxLatestItems@home','12','STRING'),('osclass','maxResultsPerPage@search','50','INTEGER'),('osclass','maxSizeKb','2048','INTEGER'),('osclass','moderate_comments','0','INTEGER'),('osclass','moderate_items','-1','INTEGER'),('osclass','mod_rewrite_loaded','0','BOOLEAN'),('osclass','notify_contact_friends','1','BOOLEAN'),('osclass','notify_contact_item','1','BOOLEAN'),('osclass','notify_new_comment','1','BOOLEAN'),('osclass','notify_new_comment_user','0','BOOLEAN'),('osclass','notify_new_item','1','BOOLEAN'),('osclass','notify_new_user','1','BOOLEAN'),('osclass','numImages@items','4','INTEGER'),('osclass','num_rss_items','50','STRING'),('osclass','osclass_installed','1','BOOLEAN'),('osclass','pageDesc','','STRING'),('osclass','pageTitle','Pasar Raya - Pasar Kuliner Terbesar','STRING'),('osclass','ping_search_engines','1','BOOLEAN'),('osclass','plugins_downloaded','[\"http:\\/\\/www.osclass.org\\/files\\/plugins\\/google_analytics\\/update.php\",\"http:\\/\\/www.osclass.org\\/files\\/plugins\\/google_maps\\/update.php\"]','STRING'),('osclass','plugins_last_version_check','1429238078','STRING'),('osclass','plugins_to_update','[]','STRING'),('osclass','plugins_update_count','0','STRING'),('osclass','purge_latest_searches','1000','STRING'),('osclass','recaptchaPrivKey','','STRING'),('osclass','recaptchaPubKey','','STRING'),('osclass','reg_user_can_contact','0','BOOLEAN'),('osclass','reg_user_post','0','BOOLEAN'),('osclass','reg_user_post_comments','0','BOOLEAN'),('osclass','rewriteEnabled','0','BOOLEAN'),('osclass','rewrite_cat_url','{CATEGORIES}','STRING'),('osclass','rewrite_contact','contact','STRING'),('osclass','rewrite_feed','feed','STRING'),('osclass','rewrite_item_activate','item/activate','STRING'),('osclass','rewrite_item_contact','item/contact','STRING'),('osclass','rewrite_item_delete','item/delete','STRING'),('osclass','rewrite_item_edit','item/edit','STRING'),('osclass','rewrite_item_mark','item/mark','STRING'),('osclass','rewrite_item_new','item/new','STRING'),('osclass','rewrite_item_resource_delete','resource/delete','STRING'),('osclass','rewrite_item_send_friend','item/send-friend','STRING'),('osclass','rewrite_item_url','{CATEGORIES}/{ITEM_TITLE}_i{ITEM_ID}','STRING'),('osclass','rewrite_language','language','STRING'),('osclass','rewrite_page_url','{PAGE_SLUG}-p{PAGE_ID}','STRING'),('osclass','rewrite_rules','','STRING'),('osclass','rewrite_search_category','category','STRING'),('osclass','rewrite_search_city','city','STRING'),('osclass','rewrite_search_city_area','cityarea','STRING'),('osclass','rewrite_search_country','country','STRING'),('osclass','rewrite_search_pattern','pattern','STRING'),('osclass','rewrite_search_region','region','STRING'),('osclass','rewrite_search_url','search','STRING'),('osclass','rewrite_search_user','user','STRING'),('osclass','rewrite_user_activate','user/activate','STRING'),('osclass','rewrite_user_activate_alert','alert/confirm','STRING'),('osclass','rewrite_user_alerts','user/alerts','STRING'),('osclass','rewrite_user_change_email','email/change','STRING'),('osclass','rewrite_user_change_email_confirm','email/confirm','STRING'),('osclass','rewrite_user_change_password','password/change','STRING'),('osclass','rewrite_user_change_username','username/change','STRING'),('osclass','rewrite_user_dashboard','user/dashboard','STRING'),('osclass','rewrite_user_forgot','user/forgot','STRING'),('osclass','rewrite_user_items','user/items','STRING'),('osclass','rewrite_user_login','user/login','STRING'),('osclass','rewrite_user_logout','user/logout','STRING'),('osclass','rewrite_user_profile','user/profile','STRING'),('osclass','rewrite_user_recover','user/recover','STRING'),('osclass','rewrite_user_register','user/register','STRING'),('osclass','save_latest_searches','0','BOOLEAN'),('osclass','selectable_parent_categories','','STRING'),('osclass','seo_url_search_prefix','','STRING'),('osclass','subdomain_host','','STRING'),('osclass','subdomain_type','','STRING'),('osclass','theme','bender','STRING'),('osclass','themes_downloaded','[\"bender\"]','STRING'),('osclass','themes_last_version_check','1429238077','STRING'),('osclass','themes_to_update','[]','STRING'),('osclass','themes_update_count','0','STRING'),('osclass','timeFormat','g:i a','STRING'),('osclass','timezone','Europe/Madrid','STRING'),('osclass','title_character_length','100','INTEGER'),('osclass','update_core_json','','STRING'),('osclass','username_blacklist','admin,user','STRING'),('osclass','use_imagick','0','BOOLEAN'),('osclass','version','356','INTEGER'),('osclass','warn_expiration','0','INTEGER'),('osclass','watermark_image','','STRING'),('osclass','watermark_place','centre','STRING'),('osclass','watermark_text','','STRING'),('osclass','watermark_text_color','','STRING'),('osclass','weekStart','0','STRING');
/*!40000 ALTER TABLE `pr_t_preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_region`
--

DROP TABLE IF EXISTS `pr_t_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_region` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_c_country_code` char(2) NOT NULL,
  `s_name` varchar(60) NOT NULL,
  `s_slug` varchar(60) NOT NULL DEFAULT '',
  `b_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pk_i_id`),
  KEY `fk_c_country_code` (`fk_c_country_code`),
  KEY `idx_s_name` (`s_name`),
  KEY `idx_s_slug` (`s_slug`),
  CONSTRAINT `pr_t_region_ibfk_1` FOREIGN KEY (`fk_c_country_code`) REFERENCES `pr_t_country` (`pk_c_code`)
) ENGINE=InnoDB AUTO_INCREMENT=781464 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_region`
--

LOCK TABLES `pr_t_region` WRITE;
/*!40000 ALTER TABLE `pr_t_region` DISABLE KEYS */;
INSERT INTO `pr_t_region` VALUES (781431,'id','Aceh','aceh',1),(781432,'id','Bali','bali',1),(781433,'id','Bangka Belitung Islands','bangka-belitung-islands',1),(781434,'id','Banten','banten',1),(781435,'id','Bengkulu','bengkulu',1),(781436,'id','Central Java','central-java',1),(781437,'id','Central Kalimantan','central-kalimantan',1),(781438,'id','Central Sulawesi','central-sulawesi',1),(781439,'id','East Java','east-java',1),(781440,'id','East Kalimantan','east-kalimantan',1),(781441,'id','East Nusa Tenggara','east-nusa-tenggara',1),(781442,'id','Gorontalo','gorontalo',1),(781443,'id','Jakarta','jakarta',1),(781444,'id','Jambi','jambi',1),(781445,'id','Lampung','lampung',1),(781446,'id','Maluku','maluku',1),(781447,'id','North Maluku','north-maluku',1),(781448,'id','North Sulawesi','north-sulawesi',1),(781449,'id','North Sumatra','north-sumatra',1),(781450,'id','Papua','papua',1),(781451,'id','Riau','riau',1),(781452,'id','Riau Islands','riau-islands',1),(781453,'id','South East Sulawesi','south-east-sulawesi',1),(781454,'id','South Kalimantan','south-kalimantan',1),(781455,'id','South Sulawesi','south-sulawesi',1),(781456,'id','South Sumatra','south-sumatra',1),(781457,'id','West Java','west-java',1),(781458,'id','West Kalimantan','west-kalimantan',1),(781459,'id','West Nusa Tenggara','west-nusa-tenggara',1),(781460,'id','West Papua','west-papua',1),(781461,'id','West Sulawesi','west-sulawesi',1),(781462,'id','West Sumatra','west-sumatra',1),(781463,'id','Yogyakarta','yogyakarta',1);
/*!40000 ALTER TABLE `pr_t_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_region_stats`
--

DROP TABLE IF EXISTS `pr_t_region_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_region_stats` (
  `fk_i_region_id` int(10) unsigned NOT NULL,
  `i_num_items` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fk_i_region_id`),
  KEY `idx_num_items` (`i_num_items`),
  CONSTRAINT `pr_t_region_stats_ibfk_1` FOREIGN KEY (`fk_i_region_id`) REFERENCES `pr_t_region` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_region_stats`
--

LOCK TABLES `pr_t_region_stats` WRITE;
/*!40000 ALTER TABLE `pr_t_region_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_region_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_user`
--

DROP TABLE IF EXISTS `pr_t_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_user` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dt_reg_date` datetime NOT NULL,
  `dt_mod_date` datetime DEFAULT NULL,
  `s_name` varchar(100) NOT NULL,
  `s_username` varchar(100) NOT NULL,
  `s_password` char(60) NOT NULL,
  `s_secret` varchar(40) DEFAULT NULL,
  `s_email` varchar(100) NOT NULL,
  `s_website` varchar(100) DEFAULT NULL,
  `s_phone_land` varchar(45) DEFAULT NULL,
  `s_phone_mobile` varchar(45) DEFAULT NULL,
  `b_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `b_active` tinyint(1) NOT NULL DEFAULT '0',
  `s_pass_code` varchar(100) DEFAULT NULL,
  `s_pass_date` datetime DEFAULT NULL,
  `s_pass_ip` varchar(15) DEFAULT NULL,
  `fk_c_country_code` char(2) DEFAULT NULL,
  `s_country` varchar(40) DEFAULT NULL,
  `s_address` varchar(100) DEFAULT NULL,
  `s_zip` varchar(15) DEFAULT NULL,
  `fk_i_region_id` int(10) unsigned DEFAULT NULL,
  `s_region` varchar(100) DEFAULT NULL,
  `fk_i_city_id` int(10) unsigned DEFAULT NULL,
  `s_city` varchar(100) DEFAULT NULL,
  `fk_i_city_area_id` int(10) unsigned DEFAULT NULL,
  `s_city_area` varchar(200) DEFAULT NULL,
  `d_coord_lat` decimal(10,6) DEFAULT NULL,
  `d_coord_long` decimal(10,6) DEFAULT NULL,
  `b_company` tinyint(1) NOT NULL DEFAULT '0',
  `i_items` int(10) unsigned DEFAULT '0',
  `i_comments` int(10) unsigned DEFAULT '0',
  `dt_access_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `s_access_ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`pk_i_id`),
  UNIQUE KEY `s_email` (`s_email`),
  KEY `idx_s_name` (`s_name`(6)),
  KEY `idx_s_username` (`s_username`),
  KEY `fk_c_country_code` (`fk_c_country_code`),
  KEY `fk_i_region_id` (`fk_i_region_id`),
  KEY `fk_i_city_id` (`fk_i_city_id`),
  KEY `fk_i_city_area_id` (`fk_i_city_area_id`),
  CONSTRAINT `pr_t_user_ibfk_1` FOREIGN KEY (`fk_c_country_code`) REFERENCES `pr_t_country` (`pk_c_code`),
  CONSTRAINT `pr_t_user_ibfk_2` FOREIGN KEY (`fk_i_region_id`) REFERENCES `pr_t_region` (`pk_i_id`),
  CONSTRAINT `pr_t_user_ibfk_3` FOREIGN KEY (`fk_i_city_id`) REFERENCES `pr_t_city` (`pk_i_id`),
  CONSTRAINT `pr_t_user_ibfk_4` FOREIGN KEY (`fk_i_city_area_id`) REFERENCES `pr_t_city_area` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_user`
--

LOCK TABLES `pr_t_user` WRITE;
/*!40000 ALTER TABLE `pr_t_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_user_description`
--

DROP TABLE IF EXISTS `pr_t_user_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_user_description` (
  `fk_i_user_id` int(10) unsigned NOT NULL,
  `fk_c_locale_code` char(5) NOT NULL,
  `s_info` text,
  PRIMARY KEY (`fk_i_user_id`,`fk_c_locale_code`),
  KEY `fk_c_locale_code` (`fk_c_locale_code`),
  CONSTRAINT `pr_t_user_description_ibfk_1` FOREIGN KEY (`fk_i_user_id`) REFERENCES `pr_t_user` (`pk_i_id`),
  CONSTRAINT `pr_t_user_description_ibfk_2` FOREIGN KEY (`fk_c_locale_code`) REFERENCES `pr_t_locale` (`pk_c_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_user_description`
--

LOCK TABLES `pr_t_user_description` WRITE;
/*!40000 ALTER TABLE `pr_t_user_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_user_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_user_email_tmp`
--

DROP TABLE IF EXISTS `pr_t_user_email_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_user_email_tmp` (
  `fk_i_user_id` int(10) unsigned NOT NULL,
  `s_new_email` varchar(100) NOT NULL,
  `dt_date` datetime NOT NULL,
  PRIMARY KEY (`fk_i_user_id`),
  CONSTRAINT `pr_t_user_email_tmp_ibfk_1` FOREIGN KEY (`fk_i_user_id`) REFERENCES `pr_t_user` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_user_email_tmp`
--

LOCK TABLES `pr_t_user_email_tmp` WRITE;
/*!40000 ALTER TABLE `pr_t_user_email_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_user_email_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_t_widget`
--

DROP TABLE IF EXISTS `pr_t_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_t_widget` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_description` varchar(40) NOT NULL,
  `s_location` varchar(40) NOT NULL,
  `e_kind` enum('TEXT','HTML') NOT NULL,
  `s_content` mediumtext NOT NULL,
  PRIMARY KEY (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_t_widget`
--

LOCK TABLES `pr_t_widget` WRITE;
/*!40000 ALTER TABLE `pr_t_widget` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_t_widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'pasarraya'
--

--
-- Dumping routines for database 'pasarraya'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-17 11:07:34
